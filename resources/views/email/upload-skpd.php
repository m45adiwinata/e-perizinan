<b>Halo, {{$data['nama']}}!</b>
<p>
Bersama ini kami kirimkan softcopy SKPD untuk permohonan Izin Reklame dengan nomor registrasi {{$data['no_registrasi']}} 
sebesar Rp. {{number_format($data['reklame_pembayaran']['nominal'])}}.
</p>
<p>
Batas akhir pembayaran SKPD ini jatuh pada tanggal sesuai yang tertera pada file SKPD. Mohon agar SKPD 
ini dapat segera dibayar agar bisa kami proses.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>
<br>
<hr>
<small>
</small>