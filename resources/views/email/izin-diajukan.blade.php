<b>Halo, {{$data['nama']}}!</b>
<p>
Pada tanggal {{date('j F Y', strtotime($data['created_at']))}} pukul {{date('H:i:s', strtotime($data['created_at']))}} WITA, sistem kami telah menerima permohonan {{$data['nama_jenis_izin']}} melalui akun Si Ajaib Anda.
</p>
<p>
Silahkan pantau terus status permohonan izin {{$data['no_registrasi']}} Anda pada Dashboard akun Si Ajaib Anda. 
</p>
<p>
Di samping itu, Anda juga akan memperoleh informasi terupdate terkait status permohonan {{$data['nama_jenis_izin']}} Anda melalui email.
</p>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>
