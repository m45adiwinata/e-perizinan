<b>
Halo, {{$data['name']}}!
</b>
<p>
Anda telah melakukan permintaan untuk mereset password akun Si Ajaib Anda. Untuk melanjutkan prosesnya, silahkan klik tombol berikut untuk mengatur ulang password Anda.
</p>
<a href="http://siajaib.bulelengkab.go.id/reset-password/{{$data['id']}}">Reset Password</a>
<p>
Namun bila Anda tidak pernah meminta proses ini, maka kami berharap Anda mengabaikan email ini.
</p>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>
<br>
<hr>
<small>
Jika Anda mengalami kesulitan dalam melakukan klik tombol "Atur Ulang Password", silakan salin link ini pada peramban Anda: ${LINK}.
</small>