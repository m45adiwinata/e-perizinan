<b>Halo, {{$data['nama']}}!</b>
<p>
Terdapat satu atau lebih syarat berkas yang perlu Anda revisi untuk permohonan izin {{$data['no_registrasi']}}. Segera upload ulang syarat berkas tersebut pada Dashboard akun Si Ajaib Anda.
</p>
<a href="https://siajaib.bulelengkab.go.id/perijinan/{{$data['id']}}">Revisi Berkas</a>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>
<br>
<hr>
<small>
Jika Anda mengalami kesulitan dalam melakukan klik tombol "Verifikasi Permohonan", silakan salin link ini pada peramban Anda: https://siajaib.bulelengkab.go.id/perijinan/{{$data['id']}}.
</small>