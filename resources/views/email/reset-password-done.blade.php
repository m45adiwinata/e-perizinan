<b>Halo, {{$data['nama']}}!</b>
<p>
Password Si Ajaib untuk email {{substr($data['email'], 0, 6)}}*****@*****.com telah diubah menjadi <b>S!aja!b1234</b> pada tanggal {{date('j F Y', strtotime($data['updated_at']))}}, pukul {{date('H:i:s', strtotime($data['updated_at']))}} WITA.
</p>
<p>
Jika ini Anda, abaikan email ini. 
</p>
<p>
Jika bukan Anda yang melakukan reset password, ikuti langkah ini:
</p>
<ol>
    <li>Reset password Anda, ubah password dengan kombinasi yang belum pernah Anda gunakan sebelumnya.</li>
    <li>Hubungi Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng melalui telepon di (0362) 22063 atau email dpmptsp@bulelengkab.go.id</li>
</ol>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>