<b>Halo, {{$data['nama']}}!</b>
<p>
Surat {{$data['nama_jenis_izin']}} Anda telah terbit! Anda dapat mengakses melalui tautan berikut.
</p>
<a href="https://siajaib.bulelengkab.go.id/perijinan/{{$data['id']}}">Izin Terbit</a>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113

</p>
<br>
<hr>
<small>
Jika Anda mengalami kesulitan dalam melakukan klik tombol "Lihat Izin Terbit", silakan salin link ini pada peramban Anda: https://siajaib.bulelengkab.go.id/perijinan/{{$data['id']}}.
</small>