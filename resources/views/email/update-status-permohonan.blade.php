<b>Halo, {{$data['nama']}}!</b>
<p>
Permohonan Izin {{$data['no_registrasi']}} Anda telah diserahkan ke {{$data['nama_verifikator']}} pada tanggal {{date('j F Y', strtotime($data['updated_at']))}}, pukul {{date('G:i:s', strtotime($data['updated_at']))}} WITA.
</p>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>