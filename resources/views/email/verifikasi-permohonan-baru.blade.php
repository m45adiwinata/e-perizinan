<b>Halo, {{$data['nama_verifikator']}}!</b>
<p>
Permohonan Izin {{$data['no_registrasi']}} untuk {{$data['nama_jenis_izin']}} atas nama {{$data['nama']}} perlu diverifikasi.
</p>
<a href="https://siajaib.bulelengkab.go.id/perijinan/{{$data['id']}}">Verifikasi Permohonan</a>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>
<br>
<hr>
<small>
Jika Anda mengalami kesulitan dalam melakukan klik tombol "Verifikasi Permohonan", silakan salin link ini pada peramban Anda: https://siajaib.bulelengkab.go.id/perijinan/{{$data['id']}}.
</small>