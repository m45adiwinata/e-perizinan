<b>Halo {{$data['name']}}</b>
<p>
Anda telah memasukkan alamat email {{$data['email']}} sebagai kontak untuk akun Si Ajaib. Untuk menyelesaikan proses pendaftaran, kami akan melakukan verifikasi untuk memastikan bahwa email ini milik Anda.
</p>
<br>
<p>
Silakan klik tautan berikut dan kembali masuk menggunakan akun Si Ajaib Anda.
</p>
<a href="http://siajaib.bulelengkab.go.id/verify-email-user/{{$data['id']}}">Verifikasi</a>
<p>
Mengapa saya terima email ini?<br>
Email ini dikirimkan jika seseorang atau perubahan terjadi atas akun Si Ajaib. Jika Anda tidak melakukan perubahan apa pun, jangan khawatir. Akun Email Anda tidak dapat digunakan sebagai kontak dalam akun Si Ajaib tanpa verifikasi yang Anda lakukan.
</p>
<p>
Email ini dikirimkan secara otomatis oleh sistem, kami tidak melakukan pengecekan email yang dikirimkan ke email ini. Mohon untuk tidak membalas email ini.
</p>
<p>
Terima kasih,<br>
Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Buleleng
Jalan Ngurah Rai No. 72, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81113
</p>