<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisIzin extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'jenis_izin';
    protected $dates = ['deleted_at'];

    public function detailForm()
    {
        return $this->hasMany('App\Models\DetailForm', 'id_jenis_izin', 'id');
    }

    public function berkas()
    {
        return $this->hasMany('App\Models\BerkasJenisIzin', 'id_jenis_izin', 'id');
    }

    public function alur()
    {
        return $this->hasMany('App\Models\AlurJenisIzin', 'id_jenis_izin', 'id');
    }

    public function kelengkapan()
    {
        return $this->hasMany('App\Models\KelengkapanIzin', 'id_jenis_izin', 'id');
    }
}
