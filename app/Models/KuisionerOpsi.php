<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KuisionerOpsi extends Model
{
    use HasFactory;
    
    protected $table = 'kuisioner_opsi';
}
