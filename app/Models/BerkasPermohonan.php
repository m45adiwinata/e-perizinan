<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BerkasPermohonan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'berkas_permohonan';
    protected $fillable = ['id_permohonan', 'nama_berkas', 'file', 'required'];

    public function validasi()
    {
        return $this->hasMany('App\Models\ValidasiBerkas', 'id_berkas', 'id')
                    ->join('alur_permohonan as a', 'a.id', 'id_alur')->where('a.id_user', auth()->user()->id);
    }
}
