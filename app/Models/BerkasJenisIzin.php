<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BerkasJenisIzin extends Model
{
    use HasFactory;

    protected $table = 'berkas_jenis_izin';
    protected $fillable = ['id_jenis_izin', 'nama_berkas', 'required'];
}
