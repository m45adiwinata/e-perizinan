<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kuisioner extends Model
{
    use HasFactory;

    protected $table = 'kuisioner';

    public function jawaban()
    {
        return $this->hasMany('App\Models\KuisionerJawaban', 'id_kuisioner', 'id');
    }
}
