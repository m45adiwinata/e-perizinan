<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KuisionerPertanyaan extends Model
{
    use HasFactory;

    protected $table = 'kuisioner_pertanyaan';

    public function opsi()
    {
        return $this->hasMany('App\Models\KuisionerOpsi', 'id_pertanyaan', 'id');
    }
}
