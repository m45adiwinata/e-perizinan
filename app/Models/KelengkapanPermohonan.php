<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelengkapanPermohonan extends Model
{
    use HasFactory;
    
    protected $table = 'kelengkapan_permohonan';
    protected $guarded = [];
}
