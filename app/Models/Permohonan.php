<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permohonan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'permohonan';

    public function detail()
    {
        return $this->hasMany('App\Models\DetailPermohonan', 'id_permohonan', 'id');
    }

    public function berkas()
    {
        return $this->hasMany('App\Models\BerkasPermohonan', 'id_permohonan', 'id');
    }
    
    public function alur()
    {
        return $this->hasMany('App\Models\AlurPermohonan', 'id_permohonan', 'id');
    }

    public function kelengkapan()
    {
        return $this->hasMany('App\Models\KelengkapanPermohonan', 'id_permohonan', 'id');
    }

    public function jenisIzin()
    {
        return $this->belongsTo('App\Models\JenisIzin', 'id_jenis_izin', 'id');
    }

    public function kuisioner()
    {
        return $this->hasOne('App\Models\Kuisioner', 'id_permohonan', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user', 'id');
    }

    public function reklamePembayaran()
    {
        return $this->hasOne('App\Models\ReklamePembayaran', 'id_permohonan', 'id');
    }

    public function registrasi()
    {
        return $this->belongsTo('App\Models\RegisterReklame', 'no_registrasi', 'no_reg');
    }

    public function images()
    {
        return $this->hasMany('App\Models\PermohonanImages', 'id_permohonan', 'id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($data) { // before delete() method call this
             $data->detail()->delete();
             $data->berkas()->delete();
             $data->alur()->delete();
             $data->kelengkapan()->delete();
             // do the rest of the cleanup...
        });
    }
}
