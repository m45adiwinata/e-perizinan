<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelengkapanIzin extends Model
{
    use HasFactory;

    protected $table = 'kelengkapan_izin';
    protected $guarded = [];
    
}
