<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegisterReklame extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'registrasi_reklame';
    protected $guarded = [];

    public function permohonan()
    {
        return $this->hasMany('App\Models\Permohonan', 'no_registrasi', 'no_reg');
    }

    public function reklame()
    {
        return $this->permohonan()->where('id_jenis_izin', 9);
    }
}
