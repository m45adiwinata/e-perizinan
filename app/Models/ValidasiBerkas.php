<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValidasiBerkas extends Model
{
    use HasFactory;
    
    protected $table = 'validasi_berkas';
    public $timestamps = false;
    protected $guarded = [];
}
