<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlurJenisIzin extends Model
{
    use HasFactory;

    protected $table = 'alur';
    protected $fillable = ['id_jenis_izin', 'id_user', 'nama', 'upload', 'no_urut'];
}
