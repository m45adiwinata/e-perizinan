<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KuisionerJawaban extends Model
{
    use HasFactory;
    protected $table = 'kuisioner_jawaban';

    public function opsi()
    {
        return $this->hasOne('App\Models\KuisionerOpsi', 'id', 'id_opsi');
    }
}
