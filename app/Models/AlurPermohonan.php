<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlurPermohonan extends Model
{
    use HasFactory;

    protected $table = 'alur_permohonan';
    // protected $fillable = ['id_permohonan', 'id_user', 'nama', 'upload', 'no_urut', 'as_penandatangan'];
    protected $guarded = [];

    public function permohonan()
    {
        return $this->belongsTo('App\Models\Permohonan', 'id_permohonan', 'id');
    }
}
