<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        if (auth()->user()->id_role == 1) {
            $data = DB::table('users as u')->selectRaw('u.id, u.name, u.jenis_kelamin, u.telepon, u.nik, u.alamat, u.email, u.id_role');
            if (isset($request->name)) {
                $data = $data->where('u.name', 'like', '%'.$request->name.'%');
            }
            if (isset($request->order)) {
                if (!isset($request->sort)) {
                    $data = $data->orderBy('u.id', $request->order);
                }
                else {
                    $data = $data->orderBy($request->sort, $request->order);
                }
            }
            else {
                if (!isset($request->sort)) {
                    $data = $data->orderBy('u.id', 'asc');
                }
                else {
                    $data = $data->orderBy($request->sort, 'asc');
                }
            }
            if (isset($request->limit)) {
                $data = $data->limit($request->limit);
            }

            return response()->json($data->get(), 200);
        }
        
        return response()->json(['message' => 'User anda tidak berhak mengakses menu ini.'], 401);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id' => 'required',
            'name' => 'required',
            'jenis_kelamin' => 'required',
            'nik' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'id_role' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        $temp = DB::table('users')->where('id', '!=', $request->input('id'))->where('email', $request->input('email'))->first();
        if ($temp) {
            return response()->json(['error' => 'Email sudah digunakan.'], 401);
        }
        $data = DB::table('users')->where('id', $request->input('id'))->update([
            'name' => $request->input('name'),
            'jenis_kelamin' => $request->input('jenis_kelamin'),
            'nik' => $request->input('nik'),
            'alamat' => $request->input('alamat'),
            'email' => $request->input('email'),
            'id_role' => $request->input('id_role')
        ]);

        return response()->json(['message' => 'User berhasil diupdate'], 201);
    }

    public function delete($id)
    {
        DB::table('users')->where('id', $id)->delete();

        return response()->json(['message' => 'User berhasil dihapus'], 201);
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'email' => 'required',
            'password' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        DB::table('users')->where('email', $request->email)->update(['password' => Hash::make($request->password)]);

        return response()->json(['message' => 'Password berhasil diubah'], 201);
    }
}
