<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AlurPermohonan;
use App\Models\Permohonan;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Mail;
use App\Mail\VerifikasiPermohonanBaruMail;
use App\Mail\PermohonanPerluTTDMail;
use App\Mail\PermohonanSelesaiMail;
// use App\Mail\RevisiKePemohonMail;
use App\Mail\UpdateStatusPermohonanMail;
use App\Mail\VerifikasiUlangPermohonanMail;

class AlurPermohonanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function setStatus(Request $request)
    {
        $this->validate($request,[
            'id' => 'required',
            'status' => 'required'
        ]);
        $alur = AlurPermohonan::where('id', $request->input('id'))->with('permohonan')->first();
        $data = DB::table('alur_permohonan')->where('id', $request->input('id'));
        if ($request->input('status') == 1) {
            DB::table('detail_permohonan')->where('id_permohonan', $alur->permohonan->id)
                ->where('id_user_revisi', $alur->id_user)
                ->update(['id_user_revisi' => null, 'ket_revisi' => null]);
            DB::table('berkas_permohonan')->where('id_permohonan', $alur->permohonan->id)
                ->where('id_user_revisi', $alur->id_user)
                ->update(['id_user_revisi' => null, 'ket_revisi' => null]);
            $data->update(['status' => $request->input('status')]);
            
        }
        $temp = DB::table('alur_permohonan')->select('status')->where('id_permohonan', $alur->permohonan->id)->groupBy('status')->get();
        $errors = array();
        if (count($temp) == 1 && $temp[0]->status == 1) {
            DB::table('permohonan')->where('id', $alur->permohonan->id)->update(['status' => 'Selesai', 'urutan_alur' => $alur->user_id]);
            $permohonan = DB::table('permohonan as p')
                            ->select('p.*', 'u.email')
                            ->join('users as u', 'p.id_user', 'u.id')
                            ->where('p.id', $alur->permohonan->id)->first();
            try {
                Mail::to($permohonan->email)->send(new PermohonanSelesaiMail(collect($permohonan)));
            } catch (\Throwable $th) {
                array_push($errors, $th->getMessage());
            }
        }
        else if ($request->input('status') == 1) {
            $next_verifikator = AlurPermohonan::where('id_permohonan', $alur->permohonan->id)->where('no_urut', $alur->no_urut+1)->first();
            if ($next_verifikator) {
                DB::table('permohonan')->where('id', $alur->permohonan->id)->update(['urutan_alur' => $next_verifikator->id_user]);
                $permohonan = DB::table('permohonan as p')
                                ->select('p.*', 'u.email', 'u2.email as email_verifikator', 'u2.name as nama_verifikator')
                                ->join('users as u', 'p.id_user', 'u.id')
                                ->join('users as u2', 'u2.id', 'p.urutan_alur')
                                ->where('p.id', $alur->permohonan->id)->first();
                if ($next_verifikator->as_penandatangan != 1) {
                    $dp = DB::table('detail_permohonan')->where('id_permohonan', $alur->permohonan->id)
                            ->where('id_user_revisi', $next_verifikator->id_user)
                            ->get();
                    $bp = DB::table('berkas_permohonan')->where('id_permohonan', $alur->permohonan->id)
                            ->where('id_user_revisi', $next_verifikator->id_user)
                            ->get();
                    if (count($dp) == 0 && count($bp) == 0) {
                        try {
                            Mail::to($permohonan->email_verifikator)->send(new VerifikasiPermohonanBaruMail(collect($permohonan)));
                        } catch (\Throwable $th) {
                            array_push($errors, $th->getMessage());
                        }
                    } else {
                        try {
                            Mail::to($permohonan->email_verifikator)->send(new VerifikasiUlangPermohonanMail(collect($permohonan)));
                        } catch (\Throwable $th) {
                            array_push($errors, $th->getMessage());
                        }
                    }
                }
                else {
                    try {
                        Mail::to($permohonan->email_verifikator)->send(new PermohonanPerluTTDMail(collect($permohonan)));
                    } catch (\Throwable $th) {
                        array_push($errors, $th->getMessage());
                    }
                }
                try {
                    Mail::to($permohonan->email)->send(new UpdateStatusPermohonanMail(collect($permohonan)));
                } catch (\Throwable $th) {
                    array_push($errors, $th->getMessage());
                }
            }
        }
        
        $alur = AlurPermohonan::where('id', $request->input('id'))->with('permohonan')->first();

        return response()->json(['message' => 'Berhasil melakukan verifikasi permohonan.', 'data' => $alur, 'errors' => $errors], 201);
    }

    public function revisi(Request $request)
    {
        $this->validate($request,[
            'id_permohonan' => 'required',
            'revisi_pada' => 'required'
        ]);
        $check_verifikator = DB::table('alur_permohonan')
                                ->where('id_permohonan', $request->input('id_permohonan'))
                                ->where('id_user', auth()->user()->id)
                                ->first();
        if (auth()->user()->id_role != 2 || is_null($check_verifikator)) {
            return response()->json(['error' => 'User tidak memiliki akses untuk revisi permohonan ini.'], 401);
        }
        if ($request->input('revisi_pada') == 'detail') {
            if (isset($request->id_detail) && isset($request->ket_revisi)) {
                DB::table('detail_permohonan')->where('id', $request->input('id_detail'))->update([
                    'id_user_revisi' => auth()->user()->id,
                    'ket_revisi' => $request->input('ket_revisi')
                ]);
                $revisi = DB::table('detail_permohonan')->where('id', $request->input('id_detail'))->first();
            }
            else {
                return response()->json(['error' => 'Request detail permohonan kurang lengkap.'], 401);
            }
        }
        else if ($request->input('revisi_pada') == 'berkas') {
            if (isset($request->id_berkas) && isset($request->ket_revisi)) {
                DB::table('berkas_permohonan')->where('id', $request->input('id_berkas'))->update([
                    'id_user_revisi' => auth()->user()->id,
                    'ket_revisi' => $request->input('ket_revisi')
                ]);
                $revisi = DB::table('berkas_permohonan')->where('id', $request->input('id_berkas'))->first();
            }
            else {
                return response()->json(['error' => 'Request berkas permohonan kurang lengkap.'], 400);
            }
        }
        // $permohonan = DB::table('permohonan as p')
        //                 ->select('p.*', 'u.email')
        //                 ->join('users as u', 'p.id_user', 'u.id')
        //                 ->where('p.id', $alur->permohonan->id)->first();
        // Mail::to($permohonan->email)->send(new RevisiKePemohonMail(collect($permohonan)));
        // $verifikator_pertama = AlurPermohonan::where('id_permohonan', $request->input('id_permohonan'))->orderBy('no_urut')->first();
        // DB::table('permohonan')->where('id', $request->input('id_permohonan'))->update(['urutan_alur' => $verifikator_pertama->id_user]);
        // DB::table('permohonan as p')->leftJoin('alur_permohonan as a', function($join) {
        //     $join->on('a.id_permohonan', 'p.id')->orderBy('a.no_urut')->first();
        // })->where('p.id', $request->input('id_permohonan'))->update(['p.status' => 'Revisi', 'p.urutan_alur' => DB::raw('a.id_user')]);

        return response()->json(['message' => 'Berhasil mengajukan revisi.', 'revisi' => $revisi], 201);
    }
    
    public function pembatalanRevisi(Request $request)
    {
        $this->validate($request,[
            'id_permohonan' => 'required',
            'revisi_pada' => 'required'
        ]);
        $no_urut_user = DB::table('alur_permohonan')->where('id_permohonan', $request->input('id_permohonan'))->where('id_user', auth()->user()->id)->first()->no_urut;
        if ($request->input('revisi_pada') == 'detail') {
            if (isset($request->id_detail)) {
                DB::table('detail_permohonan')->where('id', $request->input('id_detail'))->update([
                    'id_user_revisi' => null,
                    'ket_revisi' => null
                ]);
                $revisi = DB::table('detail_permohonan')->where('id', $request->input('id_detail'))->first();
            }
            else {
                return response()->json(['error' => 'Request detail permohonan kurang lengkap.'], 401);
            }
        }
        else if ($request->input('revisi_pada') == 'berkas') {
            if (isset($request->id_berkas)) {
                DB::table('berkas_permohonan')->where('id', $request->input('id_berkas'))->update([
                    'id_user_revisi' => null,
                    'ket_revisi' => null
                ]);
                DB::table('validasi_berkas as v')->join('alur_permohonan as a', 'a.id', 'v.id_alur')
                    ->where('v.id_berkas', $request->input('id_berkas'))
                    ->where('a.no_urut', '<', $no_urut_user)
                    ->update(['status_valid' => 1]);
                $revisi = DB::table('berkas_permohonan')->where('id', $request->input('id_berkas'))->first();
            }
            else {
                return response()->json(['error' => 'Request berkas permohonan kurang lengkap.'], 401);
            }
        }
        DB::table('alur_permohonan')->where('id_permohonan', $request->input('id_permohonan'))
            ->where('no_urut', '<', $no_urut_user)
            ->update(['status' => 1]);
        
        return response()->json(['message' => 'Berhasil membatalkan revisi.', 'revisi' => $revisi], 201);
    }

    public function validasiBerkas(Request $request)
    {
        $this->validate($request,[
            'id_berkas' => 'required',
            'status_valid' => 'required'
        ]);
        $check_vb = DB::table('validasi_berkas as v')
                    ->select('v.id')
                    ->join('berkas_permohonan as b', 'v.id_berkas', 'b.id')
                    ->join('alur_permohonan as a', 'v.id_alur', 'a.id')
                    ->where('b.id', $request->input('id_berkas'))
                    ->where('a.id_user', auth()->user()->id)
                    ->first();
        if (!$check_vb) {
            return response()->json(['message' => 'Validasi berkas gagal, tidak ditemukan validasi untuk id berkas yang diinput.'], 404);
        }
        $data = DB::table('validasi_berkas as v')
                    ->join('berkas_permohonan as b', 'v.id_berkas', 'b.id')
                    ->join('alur_permohonan as a', 'v.id_alur', 'a.id')
                    ->where('b.id', $request->input('id_berkas'))
                    ->where('a.id_user', auth()->user()->id)
                    ->update(['status_valid' => $request->input('status_valid')]);
        if ($request->input('status_valid') == 1) {
            DB::table('berkas_permohonan')->where('id', $request->input('id_berkas'))->where('id_user_revisi', auth()->user()->id)->update(['id_user_revisi' => null, 'ket_revisi' => null]);
        }
        
        return response()->json(['message' => 'Validasi berkas sukses.'], 201);
    }

    public function isiKelengkapan(Request $request)
    {
        $this->validate($request,[
            'id_permohonan' => 'required',
            'kelengkapan' => 'required'
        ]);
        foreach ($request->input('kelengkapan') as $key => $k) {
            DB::table('kelengkapan_permohonan')->where('id', $k['id'])->update(['value' => $k['value']]);
        }

        return response()->json([
            'message' => 'Kelengkapan terkirim.', 
            'kelengkapan' => DB::table('kelengkapan_permohonan')->where('id_permohonan', $request->input('id_permohonan'))->get()
        ], 201);
    }
    
    public function uploadRekomendasi(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id' => 'required',
            'file' => 'mimes:pdf|max:4096',
            'nomor' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        $data = Permohonan::find($request->input('id'));
        if (!$data) {
            return response()->json(['message' => 'Permohonan tidak ditemukan'], 404);
        }
        if ($request->nomor == 1) {
            if(file_exists(str_replace(env('APP_URL'), ".", $data->surat_permohonan_rekomendasi))) {
                unlink(str_replace(env('APP_URL'), ".", $data->surat_permohonan_rekomendasi));
            }
            if ($file = $request->file('file')) {
                $path = $file->store('public/file_suratrekomendasi');
                $data->surat_permohonan_rekomendasi = env('APP_URL')."/storage/app/public/file_suratrekomendasi/".$file->hashName();
                $data->save();
                return response()->json([
                    "success" => true,
                    "message" => "File successfully uploaded",
                    "data" => $data
                ]);
            }
            else {
                $data->surat_permohonan_rekomendasi = null;
                $data->save();
                return response()->json([
                    "success" => true,
                    "message" => "File surat permohonan rekomendasi telah dihapus.",
                    "data" => $data
                ]);
            }
        }
        else {
            if(file_exists(str_replace(env('APP_URL'), ".", $data->surat_rekomendasi))) {
                unlink(str_replace(env('APP_URL'), ".", $data->surat_rekomendasi));
            }
            if ($file = $request->file('file')) {
                $path = $file->store('public/file_suratrekomendasi');
                $data->surat_rekomendasi = env('APP_URL')."/storage/app/public/file_suratrekomendasi/".$file->hashName();
                $data->save();
                return response()->json([
                    "success" => true,
                    "message" => "File successfully uploaded",
                    "data" => $data
                ]);
            }
            else {
                $data->surat_rekomendasi = null;
                $data->save();
                return response()->json([
                    "success" => true,
                    "message" => "File surat rekomendasi telah dihapus.",
                    "data" => $data
                ]);
            }
        }
    }

    public function signDokumen(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id_permohonan' => 'required',
            'passphrase' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $permohonan = Permohonan::find($request->input('id_permohonan'));
        if (!$permohonan->template_pdf) {
            return response()->json(['message' => 'File pdf belum diupload bagian kelengkapan.'], 404);
        }
        $user = auth()->user();
        $file = file_get_contents(str_replace(env('APP_URL'), public_path(), $permohonan->template_pdf));
        // $permohonan_qr = DB::table('permohonan_qr')->where('id_permohonan', $permohonan->id)->first();
        // if (!$permohonan_qr) {
        //     DB::table('permohonan_qr')->insert(['id_permohonan' => $permohonan->id, 'id_encrypted' => md5($permohonan->id)]);
        // }
        $response = Http::withBasicAuth('esign', 'qwerty')
            ->attach('file', $file, 'test.pdf')
            ->post('http://10.33.6.223/api/sign/pdf', [
            'nik' => $user->nik,
            'passphrase' => $request->input('passphrase'),
            'tampilan' => 'visible',
            'page' => '1',
            'image' => 'false',
            'linkQR' => 'https://siajaib.bulelengkab.go.id/perijinan/'.$permohonan->id,
            'xAxis' => '20',
            'yAxis' => '-10',
            'width' => '150',
            'height' => '75',
            'tag' => ''
        ]);
        if ($response->status() != 200) {
            return response()->json(['message' => $response->object()->error], 400);
        }
        $filename = explode('/', $permohonan->template_pdf);
        $filename = $filename[count($filename)-1];
        Storage::disk('local')->put('public/file_templatepermohonan/'.$filename, (string) $response->getBody());
        return response()->json(['message' => 'Surat permohonan berhasil ditandatangani.', 'permohonan' => $permohonan], 200);
    }
}
