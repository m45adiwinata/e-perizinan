<?php

namespace App\Http\Controllers;

use App\Models\Kuisioner;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\KuisionerPertanyaan;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
class KuisionerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'laporanSurvey', 'saveSKM']]);
    }

    public function index()
    {
        $pertanyaans = KuisionerPertanyaan::with(['opsi' => function ($q) {
            $q->select('id', 'id_pertanyaan', 'opsi', 'point');
        }])->select('id', 'pertanyaan')->get();
        return response()->json(['pertanyaan'=>$pertanyaans], 200);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id_permohonan' => 'required',
            'tanggal' => 'required',
            'jam' => 'required',
            'jenis_kelamin' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'jenis_layanan' => 'required',
            'jawaban' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }

        $cntPertanyaan = DB::table('kuisioner_pertanyaan')->count();
        if (count($request->input('jawaban')) != $cntPertanyaan) {
            return response()->json(['error'=>'jawaban kurang lengkap'], 401);
        }
        $datenow = date("y-m-d H:i:s");
        $id = DB::table('kuisioner')->insertGetId([
            'id_permohonan' => $request->input('id_permohonan'),
            'tanggal' => $request->input('tanggal'),
            'jam' => $request->input('jam'),
            'jenis_kelamin' => $request->input('jenis_kelamin'),
            'pendidikan' => $request->input('pendidikan'),
            'pekerjaan' => $request->input('pekerjaan'),
            'jenis_layanan' => $request->input('jenis_layanan'),
            'created_at' => $datenow,
            'updated_at' => $datenow
        ]);
        DB::transaction(function () use ($request, $id, $datenow) {
            $jawaban_valid = true;
            $jawabans = array_map(function ($d) use ($id, $datenow) {
                if (!isset($d['id_opsi'])) {
                    $jawaban_valid = false;
                }
                $d['id_kuisioner'] = $id;
                $d['created_at'] = $datenow;
                $d['updated_at'] = $datenow;
                return $d;
            }, $request->input('jawaban'));
            if (!$jawaban_valid) {
                return response()->json(['error'=>'jawaban kurang lengkap'], 401);
            }
            DB::table('kuisioner_jawaban')->insert($jawabans);
            DB::table('permohonan')->where('id', $request->input('id_permohonan'))->update(['sudahquisioner' => 1]);
        });

        $data = DB::table('kuisioner_pertanyaan')->select('id', 'pertanyaan')->get();
        $arrIdPertanyaan = array_map(function ($d) {
            return $d->id;
        }, $data->toArray());
        $jawabans = DB::table('kuisioner_jawaban as kj')
                            ->select('ko.id_pertanyaan','ko.opsi', 'ko.point')
                            ->join('kuisioner_opsi as ko', 'ko.id', 'kj.id_opsi')
                            ->where('kj.id_kuisioner', $id)
                            ->get();

        foreach ($data as $key => $d) {
            $d->jawaban = array_filter($jawabans->toArray(), function ($j) use ($d){
                return $j->id_pertanyaan == $d->id;
            });
        }

        return response()->json(['message'=>'berhasil mengirim jawaban', 'data' => $data], 200);
    }

    public function saveSKM(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'nama' => 'required',
            'email' => 'required',
            'no_telp' => 'required',
            'tanggal' => 'required',
            'jam' => 'required',
            'jenis_kelamin' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'jenis_layanan' => 'required',
            'jawaban' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }

        $cntPertanyaan = DB::table('kuisioner_pertanyaan')->count();
        if (count($request->input('jawaban')) != $cntPertanyaan) {
            return response()->json(['error'=>'jawaban kurang lengkap'], 401);
        }
        $datenow = date("y-m-d H:i:s");
        $idSKM = DB::table('kepuasan_masyarakat')->insertGetId([
            'nama' => $request->input('nama'),
            'email' => $request->input('email'),
            'no_telp' => $request->input('no_telp'),
            'created_at' => $datenow,
            'updated_at' => $datenow
        ]);
        $id = DB::table('kuisioner')->insertGetId([
            'id_skm' => $idSKM,
            'tanggal' => $request->input('tanggal'),
            'jam' => $request->input('jam'),
            'jenis_kelamin' => $request->input('jenis_kelamin'),
            'pendidikan' => $request->input('pendidikan'),
            'pekerjaan' => $request->input('pekerjaan'),
            'jenis_layanan' => $request->input('jenis_layanan'),
            'created_at' => $datenow,
            'updated_at' => $datenow
        ]);
        DB::transaction(function () use ($request, $id, $datenow) {
            $jawaban_valid = true;
            $jawabans = array_map(function ($d) use ($id, $datenow) {
                if (!isset($d['id_opsi'])) {
                    $jawaban_valid = false;
                }
                $d['id_kuisioner'] = $id;
                $d['created_at'] = $datenow;
                $d['updated_at'] = $datenow;
                return $d;
            }, $request->input('jawaban'));
            if (!$jawaban_valid) {
                return response()->json(['error'=>'jawaban kurang lengkap'], 401);
            }
            DB::table('kuisioner_jawaban')->insert($jawabans);
            DB::table('permohonan')->where('id', $request->input('id_permohonan'))->update(['sudahquisioner' => 1]);
        });

        $data = DB::table('kuisioner_pertanyaan')->select('id', 'pertanyaan')->get();
        $arrIdPertanyaan = array_map(function ($d) {
            return $d->id;
        }, $data->toArray());
        $jawabans = DB::table('kuisioner_jawaban as kj')
                            ->select('ko.id_pertanyaan','ko.opsi', 'ko.point')
                            ->join('kuisioner_opsi as ko', 'ko.id', 'kj.id_opsi')
                            ->where('kj.id_kuisioner', $id)
                            ->get();

        foreach ($data as $key => $d) {
            $d->jawaban = array_filter($jawabans->toArray(), function ($j) use ($d){
                return $j->id_pertanyaan == $d->id;
            });
        }

        return response()->json(['message'=>'berhasil mengirim jawaban', 'data' => $data], 200);
    }

    public function laporanSurvey(Request $request)
    {
        $bulans = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        ];
        $validator = Validator::make($request->all(),[ 
            'tanggal_awal' => 'required|date',
            'tanggal_akhir' => 'required|date',
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        $data = Kuisioner::with(['jawaban' => function($q) {
            $q->with('opsi')->get();
        }])->whereDate('tanggal', '>=', $request['tanggal_awal'])->whereDate('tanggal', '<=', $request['tanggal_akhir']);
        $bulanTahun = $bulans[date('m', strtotime($request['tanggal_awal']))].' '.date('Y', strtotime($request['tanggal_awal']));
        $data = $data->get();
        $arrGender = array(
            'l' => ['count' => 0, 'persen' => 0.00], 
            'p' => ['count' => 0, 'persen' => 0.00]
        );
        $total = 0;
        foreach ($data as $d) {
            $total++;
            switch ($d->jenis_kelamin) {
                case 'L':
                    $arrGender['l']['count'] += 1;
                    break;
                case 'P':
                    $arrGender['p']['count'] += 1;
                    break;
                default:
                    # code...
                    break;
            }
        }
        $arrGender['l']['persen'] = $total == 0 ? 0 : ($arrGender['l']['count']/$total)*100;
        $arrGender['p']['persen'] = $total == 0 ? 0 : ($arrGender['p']['count']/$total)*100;

        ob_start();
        $styleBorder = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ],
        ];
        $styleOutline = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'FF000000'],
                ],
            ],
        ];
        $styleBold = [
            'font' => [
                'bold' => true
            ]
        ];
        $styleNilaiIKM = [
            'font' => [
                'bold' => true,
                'size' => 64
            ]
        ];

        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Laporan");
        $sheet->getColumnDimension('A')->setWidth(3);
        $sheet->getColumnDimension('B')->setWidth(10);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(20);
        $sheet->getStyle('A2:E7')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A2:E7')->getAlignment()->setVertical(Alignment::VERTICAL_TOP);
        $sheet->getStyle('B8')->getAlignment()->setVertical(Alignment::VERTICAL_TOP);
        $sheet->getStyle('D4')->getAlignment()->setWrapText(true);
        $sheet->getStyle('E4')->getAlignment()->setWrapText(true);
        $sheet->getStyle('B8')->getAlignment()->setWrapText(true);
        $sheet->mergeCells("A2:E2");
        $sheet->mergeCells("A3:E3");
        $sheet->mergeCells("B7:C7");
        $sheet->mergeCells("B8:E8");
        $sheet->getStyle("B4:E7")->applyFromArray($styleBorder);
        $sheet->getStyle("B4:E4")->applyFromArray($styleBold);

        $sheet->setCellValue('A1', '1');
        $sheet->setCellValue('B1', 'Jenis Kelamin');
        $sheet->setCellValue("A2", "Tabel 3");
        $sheet->setCellValue("A3", "Responden Menurut Karakteristik Jenis Kelamin");
        $sheet->setCellValue("B4", "Nomor");
        $sheet->setCellValue("C4", "Jenis Kelamin");
        $sheet->setCellValue("D4", "Frekuensi\n(Orang)");
        $sheet->setCellValue("E4", "Prosentase\n(%)");
        $sheet->setCellValue('B5', '1');
        $sheet->setCellValue('C5', 'Laki-laki');
        $sheet->setCellValue('D5', $arrGender['l']['count']);
        $sheet->setCellValue('E5', $arrGender['l']['persen']);
        $sheet->setCellValue('B6', '2');
        $sheet->setCellValue('C6', 'Perempuan');
        $sheet->setCellValue('D6', $arrGender['p']['count']);
        $sheet->setCellValue('E6', $arrGender['p']['persen']);
        $sheet->setCellValue('B7', 'Jumlah');
        $sheet->setCellValue('D7', $total);
        $sheet->setCellValue('E7', 100);
        $sheet->setCellValue("B8", "Sumber: Hasil Survey Kepuasan Masyarakat Dinas Penanaman Modal dan\nPelayanan Terpadu Satu Pintu Kab. Buleleng Semester ".$bulanTahun);
        $sheet->getRowDimension(8)->setRowHeight(30);

        $sheet->mergeCells("A11:E11");
        $sheet->mergeCells("A12:E12");
        $sheet->getStyle('C13')->getAlignment()->setWrapText(true);
        $sheet->getStyle('D13')->getAlignment()->setWrapText(true);
        $sheet->getStyle('E13')->getAlignment()->setWrapText(true);
        $sheet->getStyle("B13:E13")->applyFromArray($styleBold);

        $sheet->setCellValue('A10', '2');
        $sheet->setCellValue('B10', 'Pendidikan Terakhir');
        $sheet->setCellValue("A11", "Tabel 4");
        $sheet->setCellValue("A12", "Responden Menurut Karakteristik Pendidikan Terakhir");
        $sheet->setCellValue("B13", "Nomor");
        $sheet->setCellValue("C13", "Pendidikan\nTerakhir");
        $sheet->setCellValue("D13", "Frekuensi\n(Orang)");
        $sheet->setCellValue("E13", "Prosentase\n(%)");
        
        $arrPendidikan = array('SD', 'SMP', 'SMA/SMK', 'DIPLOMA', 'S1', 'S2', 'Lain-lain');
        $total = 0;
        $totalPerPendidikan = array(0,0,0,0,0,0,0);
        $persen = array();
        $iRow = 14;
        foreach ($arrPendidikan as $key => $p) {
            $temp = array_reduce($data->toArray(), function($a, $d) use ($p) {
                if ($p == 'Lain-lain') {
                    if ($d['pendidikan'] == '') {
                        return $a += 1;
                    }
                }
                if ($p == $d['pendidikan']) {
                    return $a += 1;
                }
                return $a;
            }, 0);
            $total += $temp;
            $totalPerPendidikan[$key] += $temp;
            array_push($persen, $temp);
            $sheet->setCellValue("B".$iRow, $key+1);
            $sheet->setCellValue("C".$iRow, $p);
            $sheet->setCellValue("D".$iRow, $temp);
            $iRow++;
        }
        $iRow = 14;
        foreach ($arrPendidikan as $key => $p) {
            $sheet->setCellValue("E".$iRow, ($total == 0 ? 0 : $persen[$key]/$total)*100);
            $iRow++;
        }
        $sheet->mergeCells("B$iRow:C$iRow");
        $sheet->setCellValue("B".$iRow, "Jumlah");
        $sheet->setCellValue("D".$iRow, $total);
        $sheet->setCellValue("E".$iRow, 100);
        $sheet->getStyle('A11:E'.$iRow)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A11:E'.$iRow)->getAlignment()->setVertical(Alignment::VERTICAL_TOP);
        $iRow++;
        $sheet->mergeCells("B$iRow:E$iRow");
        $sheet->getRowDimension($iRow)->setRowHeight(30);
        $sheet->getStyle('B'.$iRow)->getAlignment()->setWrapText(true);
        $sheet->setCellValue("B$iRow", "Sumber: Hasil Survey Kepuasan Masyarakat Dinas Penanaman Modal dan\nPelayanan Terpadu Satu Pintu Kab. Buleleng Semester ".$bulanTahun);
        $sheet->getStyle("B13:E".($iRow-1))->applyFromArray($styleBorder);

        $iRow += 2;
        $sheet->setCellValue('A'.$iRow, '3');
        $sheet->setCellValue('B'.$iRow, 'Pekerjaan Utama');
        $iRow++;
        $sheet->mergeCells("A$iRow:E$iRow");
        $sheet->setCellValue("A".$iRow, "Tabel 5");
        $iRow++;
        $sheet->mergeCells("A$iRow:E$iRow");
        $sheet->setCellValue("A".$iRow, "Responden Menurut Karakteristik Pekerjaan Utama");
        $iRow++;
        $sheet->setCellValue("B".$iRow, "Nomor");
        $sheet->setCellValue("C".$iRow, "Pekerjaan Utama");
        $sheet->setCellValue("D".$iRow, "Frekuensi\n(Orang)");
        $sheet->setCellValue("E".$iRow, "Prosentase\n(%)");
        $sheet->getStyle("B$iRow:E$iRow")->applyFromArray($styleBold);
        $arrPekerjaan = DB::table('pekerjaan')->select('nama_pekerjaan')->get()->toArray();
        $total = 0;
        $persen = array();
        $iRow++;
        $rowStart = $iRow;
        foreach ($arrPekerjaan as $key => $p) {
            $temp = array_reduce($data->toArray(), function($a, $d) use ($p) {
                if ($p->nama_pekerjaan == 'LAINNYA') {
                    if ($d['pekerjaan'] == '') {
                        return $a += 1;
                    }
                }
                if ($p->nama_pekerjaan == $d['pekerjaan']) {
                    return $a += 1;
                }
                return $a;
            }, 0);
            $total += $temp;
            array_push($persen, $temp);
            $sheet->setCellValue("B".$iRow, $key+1);
            $sheet->setCellValue("C".$iRow, $p->nama_pekerjaan);
            $sheet->setCellValue("D".$iRow, $temp);
            $iRow++;
        }
        $iRow = $rowStart;
        foreach ($arrPekerjaan as $key => $p) {
            $sheet->setCellValue("E".$iRow, ($total == 0 ? 0 : $persen[$key]/$total)*100);
            $iRow++;
        }
        $sheet->mergeCells("B$iRow:C$iRow");
        $sheet->setCellValue("B".$iRow, "Jumlah");
        $sheet->setCellValue("D".$iRow, $total);
        $sheet->setCellValue("E".$iRow, 100);
        $sheet->getStyle('A25:E'.$iRow)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A25:E'.$iRow)->getAlignment()->setVertical(Alignment::VERTICAL_TOP);
        $sheet->getStyle("B27:E".($iRow))->applyFromArray($styleBorder);
        $iRow++;
        $sheet->mergeCells("B$iRow:E$iRow");
        $sheet->getRowDimension($iRow)->setRowHeight(30);
        $sheet->getStyle('B'.$iRow)->getAlignment()->setWrapText(true);
        $sheet->setCellValue("B$iRow", "Sumber: Hasil Survey Kepuasan Masyarakat Dinas Penanaman Modal dan\nPelayanan Terpadu Satu Pintu Kab. Buleleng Semester ".$bulanTahun);

        $sheet = $spreadsheet->createSheet();
        $sheet->setTitle("Pengolahan");
        $maxCol = 11;
        $iRow = 1;
        $sheet->mergeCells("A$iRow:K$iRow");
        $sheet->getStyle('A'.$iRow)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($iRow)->setRowHeight(36);
        $sheet->getStyle("A".$iRow)->applyFromArray($styleBold);
        $sheet->setCellValue("A".$iRow, "PENGOLAHAN DATA SURVEY KEPUASAN MASYARAKAT PER\nRESPONDEN DAN UNSUR PELAYANAN");
        $iRow++;
        $rowHeader = $iRow;
        $sheet->mergeCells("A$iRow:A".($iRow+1));
        $sheet->setCellValue("A".$iRow, "NO RESP");
        $sheet->mergeCells("B$iRow:J".$iRow);
        $sheet->setCellValue("B".$iRow, "NILAI UNSUR PELAYANAN");
        $sheet->mergeCells("K$iRow:K".($iRow+1));
        $sheet->setCellValue("K".$iRow, "KET");
        $iRow++;
        for ($i=1; $i <= 9; $i++) {
            $sheet->setCellValue([(1+$i), $iRow], "U".$i);
        }
        $iRow++;
        for ($i=1; $i <= 11; $i++) {
            $sheet->setCellValue([($i), $iRow], $i);
        }
        $sheet->getStyle("A".$rowHeader.":K".$iRow)->applyFromArray($styleBold);
        $sheet->getStyle("A".$rowHeader.":K".$iRow)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('ffdedede');
        
        $iRow++;
        $totalPerSoal = array(0,0,0,0,0,0,0,0,0);
        foreach ($data as $key => $d) {
            $sheet->setCellValue([1,$iRow], $key+1);
            foreach ($d->jawaban as $key2 => $j) {
                $sheet->setCellValue([($key2+2),$iRow], $j->opsi->point);
                $totalPerSoal[$key2] += $j->opsi->point;
            }
            $iRow++;
        }
        $sheet->setCellValue([1,$iRow], "JML Nilai\n/ Unsur");
        foreach ($totalPerSoal as $key => $t) {
            $sheet->setCellValue([$key+2, $iRow], $t);
        }
        $iRow++;
        $sheet->setCellValue([1,$iRow], "NRR\n/ Unsur");
        foreach ($totalPerSoal as $key => $t) {
            $sheet->setCellValue([$key+2, $iRow], count($data) == 0 ? 0 : $t/count($data));
        }
        $iRow++;
        $sheet->setCellValue([1,$iRow], "NRR\nTertbg\n/ Unsur");
        foreach ($totalPerSoal as $key => $t) {
            $sheet->setCellValue([$key+2, $iRow], count($data) == 0 ? 0 : $t/(count($data)*4));
        }
        $sheet->getStyle([1,$iRow-2, 1, $iRow])->getAlignment()->setWrapText(true);

        $sheet->getStyle([1,$rowHeader-1, $maxCol, $iRow])->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle([1,$rowHeader-1, $maxCol, $iRow])->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getStyle([1,$rowHeader, $maxCol, $iRow])->applyFromArray($styleBorder);

        $sheet = $spreadsheet->createSheet();
        $sheet->setTitle("Kesimpulan");
        $iRow = 1;
        $sheet->mergeCells([1,1,8,1]);
        $sheet->getStyle('A1')->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($iRow)->setRowHeight(60);
        $sheet->setCellValue([1, 1], "INDEK KEPUASAN MASYARAKAT (IKM)\nDINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU\nKABUPATEN BULELENG\nSEMESTER ".$bulanTahun);
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $iRow+=2;
        $sheet->getColumnDimension('A')->setWidth(0);
        $sheet->getColumnDimension('B')->setWidth(48);
        $sheet->setCellValue([2, $iRow], "NILAI IKM");
        $sheet->getStyle('B'.$iRow)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B'.$iRow)->applyFromArray($styleOutline);
        $sheet->getStyle('B'.$iRow)->applyFromArray($styleBold);
        $sheet->mergeCells([4,$iRow,8,$iRow]);
        $sheet->getStyle([4,$iRow,8,$iRow])->applyFromArray($styleOutline);
        $sheet->getStyle([4,$iRow,8,$iRow])->applyFromArray($styleBold);
        $sheet->setCellValue([4, $iRow], "NAMA LAYANAN :");
        $iRow++;
        $rowStart = $iRow;
        $total = array_sum($totalPerSoal);
        $nilaiIKM = count($data) == 0 ? 0 : ($total/(9*4*count($data)))*100;
        $sheet->mergeCells([4,$iRow,8,$iRow]);
        $sheet->getRowDimension($iRow)->setRowHeight(85);
        $sheet->getStyle('D'.$iRow)->getAlignment()->setVertical(Alignment::VERTICAL_BOTTOM)->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D'.$iRow)->applyFromArray($styleBold);
        $sheet->setCellValue([4, $iRow], "RESPONDEN");
        $iRow++;
        $sheet->setCellValue([4, $iRow], "JUMLAH");
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(3);
        $sheet->getColumnDimension('F')->setWidth(15);
        $sheet->getColumnDimension('G')->setWidth(3);
        $sheet->getColumnDimension('H')->setWidth(15);
        $sheet->setCellValue([5, $iRow], ":");
        $sheet->mergeCells([6,$iRow,8,$iRow]);
        $sheet->setCellValue([6, $iRow], count($data)." Responden");
        $iRow++;
        $sheet->setCellValue([4, $iRow], "JENIS KELAMIN");
        $sheet->setCellValue([5, $iRow], ":");
        $sheet->setCellValue([6, $iRow], "L");
        $sheet->setCellValue([7, $iRow], "=");
        $sheet->setCellValue([8, $iRow], $arrGender['l']['count']." Responden");
        $iRow++;
        $sheet->setCellValue([6, $iRow], "P");
        $sheet->setCellValue([7, $iRow], "=");
        $sheet->setCellValue([8, $iRow], $arrGender['p']['count']." Responden");
        $iRow++;
        $sheet->setCellValue([4, $iRow], "PENDIDIKAN");
        $sheet->setCellValue([5, $iRow], ":");
        foreach ($arrPendidikan as $key => $p) {
            $sheet->setCellValue([6, $iRow], $p);
            $sheet->setCellValue([7, $iRow], '=');
            $sheet->setCellValue([8, $iRow], $totalPerPendidikan[$key].' Responden');
            $iRow++;
        }
        $sheet->mergeCells([4,$iRow,8,$iRow]);
        $sheet->getRowDimension($iRow)->setRowHeight(58);
    
        $sheet->setCellValue([4, $iRow], "Periode Survey (".date('d - m - Y', strtotime($request['tanggal_awal']))." s/d ".date('d - m - Y', strtotime($request['tanggal_akhir'])).")");
        
        $sheet->getStyle([4,$rowStart,8,$iRow])->applyFromArray($styleOutline);
        $sheet->getStyle('D'.$iRow)->getAlignment()->setVertical(Alignment::VERTICAL_BOTTOM)->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->mergeCells([2,$rowStart,2,$iRow]);
        $sheet->setCellValue([2,$rowStart], number_format($nilaiIKM, 2, ".", ","));
        $sheet->getStyle('B'.$rowStart)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER)->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B'.$rowStart)->applyFromArray($styleNilaiIKM);
        $sheet->getStyle([2,$rowStart,2,$iRow])->applyFromArray($styleOutline);

        try {
            ob_clean();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Laporan Survey Bulan '.$bulanTahun.' per '.date('Y-m-d').'.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
            exit(0);
        } catch (\Throwable $th) {
            return $th;
        }
    }
}
