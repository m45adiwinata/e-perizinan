<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permohonan;
use App\Models\DetailPermohonan;
use App\Models\BerkasPermohonan;
use App\Models\AlurPermohonan;
use App\Models\ValidasiBerkas;
use App\Models\PermohonanImages;
use Illuminate\Support\Facades\DB;
use App\Models\FileUploaded;
use Illuminate\Support\Facades\Log;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use PhpOffice\PhpWord\TemplateProcessor;
use Mail;
use App\Mail\MyTestMail;
use App\Mail\IzinDiajukan;
use App\Mail\VerifikasiPermohonanBaruMail;
use App\Mail\RevisiSyaratBerkasMail;
use App\Mail\VerifikasiUlangPermohonanMail;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Database\Query\Builder;

class PermohonanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['downloadTemplateWord', 'sendEmail', 'laporanBulanan']]);
    }

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'status' => 'regex:/^[A-Za-z, ]+$/',
            'no_registrasi' => 'string',
            'tanggal_pengajuan' => 'date',
            'nama_pemohon' => 'string',
            'email_pemohon' => 'regex:/^[a-z@.]+$/',
            'name' => 'regex:/^[A-Za-z ]+$/',
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $whereStatus = 'p.status IN (\'';
        if (isset($request->status)) {
            $whereStatus .= implode('\',\'', explode(',', $request->status)).'\')';
        }
        $subquery = 'SELECT a.id as id_permohonan, SUM(IF(a.`status`=1, 1,0)) AS valid_count, SUM(1) AS jml_berkas FROM (
                        SELECT p.id, vb.id_berkas, IF(id_berkas IN (
                        SELECT id_berkas FROM validasi_berkas GROUP BY id_berkas HAVING SUM(IF(status_valid = 0, 1,0)) = 0)
                        ,1,0) AS `status` FROM validasi_berkas vb INNER JOIN berkas_permohonan bp ON bp.id = vb.`id_berkas` 
                        INNER JOIN permohonan p ON bp.`id_permohonan`=p.id';
        if (isset($request->status)) {
            $subquery .= ' WHERE '.$whereStatus;
        }
        $subquery .= ' GROUP BY id_berkas, `status`) a
                        GROUP BY a.id';
        $subquery2 = 'SELECT p.id AS id_permohonan, vb.`id_alur`, SUM(IF(vb.`status_valid`=1,1,0)) AS valid_count, COUNT(vb.id_berkas) AS jml_berkas FROM validasi_berkas vb
                        INNER JOIN berkas_permohonan bp ON vb.id_berkas = bp.`id`
                        INNER JOIN permohonan p ON p.id = bp.`id_permohonan`
                        WHERE p.deleted_at IS NULL AND bp.`deleted_at` IS NULL';
        if (isset($request->status)) {
            $subquery2 .= ' AND '.$whereStatus;
        }
        $subquery2 .= ' GROUP BY p.id, vb.`id_alur`';
        $data = DB::table('permohonan as p');
        if (auth()->user()->id_role == 2) {
            $data = $data->selectRaw('p.id, p.id_user, p.id_jenis_izin, p.nama_jenis_izin, p.deskripsi_jenis_izin, p.no_registrasi, p.memohon_untuk, p.surat_kuasa,
                        p.surat_permohonan_rekomendasi, p.surat_rekomendasi, p.nama, p.nik, p.npwp, p.tempat_lahir, p.sudahquisioner, p.urutan_alur, p.template_pdf,
                        p.created_at, p.updated_at, p.deleted_at,
                        IF(p.status = "Verifikasi Ulang",
                            IF(a.status = 1, "Verifikasi", p.status),
                            IF(p.status = "Permohonan Baru",
                                IF(a.status = 1, "Verifikasi", p.status), p.status
                            )
                        )
                                AS status, u.email, u.telepon AS no_hp_pemohon, u2.name AS verifikator, a2.upload
                                , concat(vb.valid_count, "/", vb.jml_berkas) as berkas_valid, reg.id as idreg_reklame, skpd.fileskpd')
                    ->join('users as u', 'p.id_user', 'u.id')
                    ->join('alur_permohonan as a', function($join) {
                        $join->on('a.id_permohonan', '=', 'p.id')->where('a.id_user', '=', auth()->user()->id);
                    });
            $subquery = $subquery2;
        }
        else {
            $data = $data->selectRaw('p.*, u.email, u.telepon AS no_hp_pemohon, u2.name AS verifikator, a2.upload, concat(vb.valid_count, "/", vb.jml_berkas) as berkas_valid,
                    reg.id as idreg_reklame, skpd.fileskpd')
                    ->join('users as u', 'p.id_user', 'u.id')
                    ->join('alur_permohonan as a', 'a.id_permohonan', '=', 'p.id');
        }
        
        $data = $data->leftJoin('users as u2', 'p.urutan_alur', 'u2.id')
                    ->leftJoin('alur_permohonan as a2', function($join) {
                        $join->on('a2.id_permohonan', '=', 'p.id')->where('a2.id_user', '=', DB::raw('p.urutan_alur'));
                    })
                    ->leftJoin(DB::raw('('.$subquery.') vb'), function($join) {
                        if (auth()->user()->id_role == 2) {
                            $join->on('vb.id_permohonan', 'p.id')->on('vb.id_alur', 'a.id');
                        } else {
                            $join->on('vb.id_permohonan', 'p.id');
                        }
                    })
                    ->leftJoin('registrasi_reklame as reg', 'reg.no_reg', 'p.no_registrasi')
                    ->leftJoin('reklame_pembayaran as skpd', 'skpd.id_permohonan', 'p.id')
                    ->leftJoin('kelengkapan_permohonan as kp', function($join) {
                        $join->on('kp.id_permohonan', 'p.id')->where('kp.kode_isian', 'TGL_SK');
                    })
                    ->whereNull('p.deleted_at')->where('p.status', '<>', '');

        if (auth()->user()->id_role == 3) {
            $data = $data->where('p.id_user', auth()->user()->id);
        }
        if (isset($request->status)) {
            if (in_array('Verifikasi', explode(',', $request->status))) {
                $data = $data->where('a.status', 1)->whereIn('p.status', ['Permohonan Baru', 'Verifikasi Ulang']);
            }
            else if (in_array('Selesai', explode(',', $request->status))) {
                $data = $data->where('p.status', '=', 'Selesai');
            } 
            else {
                $data = $data->whereIn('p.status', explode(',', $request->status));
                if (!in_array('Revisi', explode(',', $request->status))) {
                    if (auth()->user()->id_role == 2) {
                        $data = $data->where('p.urutan_alur', '=', auth()->user()->id);
                    }
                }
            }
        }
        else {
            if (auth()->user()->id_role == 2) {
                $data = $data->where('p.urutan_alur', '=', auth()->user()->id);
            }
        }
        if (isset($request->id_user)) {
            $data = $data->where('p.id_user', $request->id_user);
        }
        if (isset($request->name)) {
            $data = $data->where('p.nama_jenis_izin', 'like', '%'.$request->name.'%');
        }
        if (isset($request->no_registrasi)) {
            $data = $data->where(function ($q) use ($request) {
                $q->where('p.no_registrasi', 'like', '%'.$request->no_registrasi.'%')
                    ->orWhere('reg.nama_perusahaan', 'like', '%'.$request->no_registrasi.'%');
            });
        }
        if (isset($request->tanggal_pengajuan)) {
            $data = $data->whereDate('p.created_at', $request->tanggal_pengajuan);
        }
        if (isset($request->nama_pemohon)) {
            $data = $data->where('p.nama', 'like', '%'.$request->nama_pemohon.'%');
        }
        if (isset($request->email_pemohon)) {
            $data = $data->where('u.email', 'like', '%'.$request->email_pemohon.'%');
        }
        if (isset($request->date_rangeSK)) {
            $temp = explode(" s/d ", $request->date_rangeSK);
            $data = $data->whereBetween('kp.value', $temp);
        }
        if (isset($request->order)) {
            if (!isset($request->sort)) {
                $data = $data->orderBy('p.id', $request->order);
            }
            else {
                $data = $data->orderBy('p.'.$request->sort, $request->order);
            }
        }
        else {
            if (!isset($request->sort)) {
                $data = $data->orderBy('p.id', 'asc');
            }
            else {
                $data = $data->orderBy('p.'.$request->sort, 'asc');
            }
        }
        $data = $data->groupBy('p.id');
        $count = DB::select('SELECT COUNT(*) as jml FROM ('.$this->getQueries($data).') a')[0]->jml;
        if (isset($request->limit)) {
            if (isset($request->pagenumber)) {
                $data = $data->offset(($request->pagenumber - 1) * $request->limit);
            }
            $data = $data->limit($request->limit);
        }
        
        $data = $data->get();
        
        $arrIdPermohonan = array_map(function ($d) {
            return $d->id;
        }, $data->toArray());
        
        $detailPermohonans = DB::table('detail_permohonan')
                                ->select('id', 'id_permohonan', 'label', 'form_type', 'value')
                                ->whereIn('id_permohonan', $arrIdPermohonan)->get();
        
        $kelengkapanPermohonans = DB::table('kelengkapan_permohonan')
                                ->select('id', 'id_permohonan', 'label', 'id_form_type', 'value')
                                ->whereIn('id_permohonan', $arrIdPermohonan)->get();
        // $query = $this->getQueries($kelengkapanPermohonans);
        // DB::listen(function ($query) {
        //     Log::info($query->sql);
        //     Log::info($query->time);
        // });
        // print_r($query);
        // exit;
        foreach ($data as $key => $d) {
            $d->detail_permohonan = array_filter($detailPermohonans->toArray(), function ($dp) use ($d){
                return $dp->id_permohonan == $d->id;
            });
            $d->kelengkapan_permohonan = array_filter($kelengkapanPermohonans->toArray(), function ($kp) use ($d){
                return $kp->id_permohonan == $d->id;
            });
        }
        // $data = $data->map(function ($value, $key) {
        //     if (!is_null($value->status_alur) && $value->status_alur == 1) {
        //         $value->status = 'Verifikasi';
        //     }

        //     return $value;
        // });
        return response()->json([
            'data' => $data,
            'limit' => isset($request->limit) ? $request->limit : null,
            'pagenumber' => isset($request->pagenumber) ? $request->pagenumber : 1, 
            'totalPages' => isset($request->limit) ? ceil($count/$request->limit) : 1,
            'countData' => $count
        ], 200);
    }

    public function view($id)
    {
        return response()->json(Permohonan::with('detail')->with(['berkas' => function($query) {
            $query->select('*')->with(['validasi' => function($q) {
                    $q->select('*');
                }])->get();
            }])
            ->with(['alur' => function($query) {
                $query->select('*')->orderBy('no_urut');
            }])
            ->with('kelengkapan')
            ->with(['user' => function($query) {
                $query->select('id', 'telepon');
            }])
            ->with('kuisioner')
            ->with('registrasi')
            ->with('reklamePembayaran')
            ->with('images')
            ->where('permohonan.id', $id)->first(), 200);
    }

    public function viewByQR($kode)
    {
        $id = DB::table('permohonan_qr')->select('id_permohonan')->where('id_encrypted', $kode)->first()->id_permohonan;
        return response()->json(Permohonan::with('detail')->with(['berkas' => function($query) {
            $query->select('*')->with(['validasi' => function($q) {
                    $q->select('*');
                }])->get();
            }])
            ->with('alur')
            ->with('kelengkapan')
            ->where('permohonan.id', $id)->first(), 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_user' => 'required',
            'id_jenis_izin' => 'required',
            'nama_jenis_izin' => 'required',
            'deskripsi_jenis_izin' => 'required',
            'memohon_untuk' => 'required',
            'nama' => 'required',
            'nik' => 'required',
            'npwp' => 'required',
            'tempat_lahir' => 'required',
            'detailForm' => 'required',
            'alur' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $kelengkapan = DB::table('kelengkapan_izin')->select('label', 'id_form_type', 'kode_isian')->where('id_jenis_izin', $request->input('id_jenis_izin'))->get();
        $kelengkapan = $kelengkapan->map(function ($value, $key) {
            return (array) $value;
        });

        $data = new Permohonan;
        $data->id_user = $request->input('id_user');
        $data->id_jenis_izin = $request->input('id_jenis_izin');
        $data->nama_jenis_izin = $request->input('nama_jenis_izin');
        $data->deskripsi_jenis_izin = $request->input('deskripsi_jenis_izin');
        $data->no_registrasi = "BLL/".time()."/".$request->input('id_jenis_izin')."/".$request->input('id_user');
        $data->memohon_untuk = $request->input('memohon_untuk');
        $data->nama = $request->input('nama');
        $data->nik = $request->input('nik');
        $data->npwp = $request->input('npwp');
        $data->tempat_lahir = $request->input('tempat_lahir');
        $data->urutan_alur = $request->input('alur')[0]['id_user'];
        $data->save();
        $data->detail()->createMany($request->input('detailForm'));
        $alur = $request->input('alur');
        $alur[count($alur)-1]['as_penandatangan'] = 1;
        $data->alur()->createMany($alur);
        if ($data->alur()->where('upload', 1)->first()) {
            $data->alur()->where('no_urut', 1)->update(['up_sp_rekomendasi' => 1]);
        }
        $data->kelengkapan()->createMany($kelengkapan);
        if ($data->id_jenis_izin == 9) {
            app('App\Http\Controllers\ReklameController')->saveFromSiAjaib([
                'id_user' => auth()->user()->id,
                'no_reg' => $data->no_registrasi,
                'nama_reg' => $data->nama,
                'nik_reg' => $data->nik,
                'npwp_reg' => $data->npwp,
                'nama_perusahaan' => $data->nama,
                'alamat_perusahaan' => "-",
                'no_telp' => "-"
            ]);
        }

        return response()->json($data->with('detail')->with('alur')->with('kelengkapan')->where('id', $data->id)->get(), 201);
    }

    public function uploadSuratKuasa(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'surat_kuasa' => 'required|mimes:pdf|max:4096'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = Permohonan::find($request->input('id'));
        if(file_exists(str_replace(env('APP_URL'), ".", $data->surat_kuasa))) {
            unlink(str_replace(env('APP_URL'), ".", $data->surat_kuasa));
        }
        if ($file = $request->file('surat_kuasa')) {
            $path = $file->store('public/file_suratkuasa');
            $data->surat_kuasa = env('APP_URL')."/storage/app/public/file_suratkuasa/".$file->hashName();
            $data->save();
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "data" => $data
            ]);
        }
    }

    public function uploadBerkas(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'id_permohonan' => 'required',
            'nama_berkas' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $permohonan = Permohonan::find($request->input('id_permohonan'));
        $data = BerkasPermohonan::where('id_permohonan', $request->input('id_permohonan'))->where('id', $request->input('id'))->first();

        if($data){
            $validator = Validator::make($request->all(),[
                'file' => $data->required ? 'required' : 'nullable' . '|mimes:pdf'
            ]);

            if($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);
            }
        }

        if(!is_null($data)) {
            if(file_exists(str_replace(env('APP_URL'), ".", $data->file))) {
                unlink(str_replace(env('APP_URL'), ".", $data->file));
            }
            $data->file = null;
            $data->save();
        }
        else {
            if ($request->input('id') != 0 && is_null($permohonan->berkas()->where('id', $request->input('id'))->first())) {
                return response()->json(['message'=> "Id berkas yang dikirim tidak valid."], 404);
            }
        }

        $berkas_izin = DB::table('berkas_jenis_izin')->where('id_jenis_izin', $permohonan->id_jenis_izin)->where('nama_berkas', $request->input('nama_berkas'))->first();
        if (!$permohonan) {
            return response()->json(['message'=> "Permohonan tidak ditemukan."], 404);
        }
        if ($file = $request->file('file')) {
            if ($file->getClientOriginalExtension() != 'pdf') {
                return response()->json(['message'=> "Berkas yang diupload harus format pdf."], 406);
            }

            $path = $file->store('public/file_berkaspermohonan');

            $new_bp = BerkasPermohonan::find($request->input('id'));
            if (!$new_bp) {
                $new_bp = new BerkasPermohonan;
            }
            $new_bp->id_permohonan = $request->input('id_permohonan');
            $new_bp->nama_berkas = $request->input('nama_berkas');
            $new_bp->file = env('APP_URL')."/storage/app/public/file_berkaspermohonan/".$file->hashName();
            $new_bp->required = $berkas_izin ? $berkas_izin->required : 0;
            $new_bp->save();
            $berkas = $new_bp;

            foreach (AlurPermohonan::where('id_permohonan', $request->input('id_permohonan'))->get(['id', 'upload']) as $a) {
                if ($a->upload == 4) {
                    $init_status_valid = 1;
                }
                else {
                    $init_status_valid = 0;
                }
                ValidasiBerkas::updateOrCreate(
                    ['id_alur' => $a->id, 'id_berkas' => $berkas->id],
                    ['id_alur' => $a->id, 'id_berkas' => $berkas->id, 'status_valid' => $init_status_valid]
                );
            }
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "data" => Permohonan::with('berkas')->where('id', $request->input('id_permohonan'))->first()
            ]);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'id_user' => 'required',
            'id_jenis_izin' => 'required',
            'nama_jenis_izin' => 'required',
            'deskripsi_jenis_izin' => 'required',
            'memohon_untuk' => 'required',
            'nama' => 'required',
            'nik' => 'required',
            'npwp' => 'required',
            'tempat_lahir' => 'required',
            'detailForm' => 'required',
            'alur' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $data = Permohonan::find($request->input('id'));
        $data->id_user = $request->input('id_user');
        $data->id_jenis_izin = $request->input('id_jenis_izin');
        $data->nama_jenis_izin = $request->input('nama_jenis_izin');
        $data->deskripsi_jenis_izin = $request->input('deskripsi_jenis_izin');
        $data->memohon_untuk = $request->input('memohon_untuk');
        $data->nama = $request->input('nama');
        $data->nik = $request->input('nik');
        $data->npwp = $request->input('npwp');
        $data->tempat_lahir = $request->input('tempat_lahir');
        $data->save();

        $this->updateDetailPermohonan($data, $request->input('detailForm'));
        $this->updateAlurPermohonan($data, $request->input('alur'));

        // $verifikator_pertama = $data->alur()->where('no_urut', 1)->first();
        // $data->urutan_alur = $verifikator_pertama ? $verifikator_pertama->id_user : 0;
        $data->save();

        return response()->json($data->with('detail')->with('berkas')->with('alur')->with('kelengkapan')->where('id', $data->id)->get(), 201);
    }

    public function delete($id)
    {
        $data = Permohonan::find($id);
        if ($data) {
            $data->delete();
            return response()->json(['message' => 'Data berhasil dihapus'], 201);
        }

        return response()->json(['message' => 'Data tidak ditemukan'], 404);
    }

    private function updateDetailPermohonan($data, $updates)
    {
        $temp = array();
        foreach ($updates as $r) {
            if ($r['id'] == 0) {
                unset($r['id']);
                $r['id_permohonan'] = $data->id;
                $newItem = DetailPermohonan::create($r);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                DB::table('detail_permohonan')->where('id_permohonan', $data->id)->where('id', $r['id'])->update($r);
            }
        }
        DB::table('detail_permohonan')->where('id_permohonan', $data->id)->whereNotIn('id', $temp)->delete();
    }

    private function updateBerkasPermohonan($data, $updates)
    {
        $temp = array();
        foreach ($updates as $r) {
            if ($r['id'] == "0") {
                unset($r['id']);
                $r['id_permohonan'] = $data->id;
                if (isset($r['file'])) {

                }
                $newItem = BerkasPermohonan::create($r);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                DB::table('berkas_permohonan')->where('id', $r['id'])->update($r);
            }
        }
        DB::table('berkas_permohonan')->where('id_permohonan', $data->id)->whereNotIn('id', $temp)->delete();
    }

    private function updateAlurPermohonan($data, $updates)
    {
        $temp = array();
        foreach ($updates as $key => $r) {
            if ($key == count($updates)-1) {
                $r['as_penandatangan'] = 1;
            }
            else {
                $r['as_penandatangan'] = 0;
            }
            if ($r['id'] == "0") {
                unset($r['id']);
                $r['id_permohonan'] = $data->id;
                $newItem = AlurPermohonan::create([
                    'id_permohonan' => $r['id_permohonan'],
                    'id_user' => $r['id_user'],
                    'nama' => $r['nama'],
                    'upload' => $r['upload'],
                    'no_urut' => $r['no_urut'],
                    'as_penandatangan' => $r['as_penandatangan']
                ]);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                DB::table('alur_permohonan')->where('id_permohonan', $data->id)->where('id', $r['id'])->update([
                    'id_user' => $r['id_user'],
                    'nama' => $r['nama'],
                    'upload' => $r['upload'],
                    'no_urut' => $r['no_urut'],
                    'as_penandatangan' => $r['as_penandatangan']
                ]);
            }
        }
        DB::table('alur_permohonan')->where('id_permohonan', $data->id)->whereNotIn('id', $temp)->delete();
        if ($data->alur()->where('upload', 1)->first()) {
            $data->alur()->where('no_urut', 1)->update(['up_sp_rekomendasi' => 1]);
            $data->alur()->where('no_urut', '>', 1)->update(['up_sp_rekomendasi' => 0]);
        }
    }

    public function setStatus(Request $request)
    {
        $this->validate($request, [
            'id_permohonan' => 'required',
            'status' => 'required'
        ]);
        $data = Permohonan::find($request->input('id_permohonan'));
        if ($request->input('status') == 'Bongkar') {
            if ($data->status != 'Tidak Aktif') {
                return response()->json([
                    'message' => 'Status permohonan tidak dapat diubah ke Bongkar sebelum status permohonan menjadi Tidak Aktif', 
                    'data' => $data], 401);
            }
        } else if ($request->input('status') == 'Dibatalkan') {
            if (auth()->user()->id_role != 1) {
                return response()->json([
                    'message' => 'Perubahan status permohonan ke Dibatalkan hanya bisa dilakukan oleh user dengan role Admin', 
                    'data' => $data], 401);
            }
        }
        $data->status = $request->input('status');
        if($request->input('status') == 'Revisi') {
            $verifikator_pertama = AlurPermohonan::where('id_permohonan', $request->input('id_permohonan'))->orderBy('no_urut')->first();
            $data->urutan_alur = $verifikator_pertama->id_user;
            DB::table('alur_permohonan')->where('id_permohonan', $request->input('id_permohonan'))->update(['status' => 0]);
            DB::table('validasi_berkas as vb')->join('berkas_permohonan as b', 'b.id', 'vb.id_berkas')
                ->join('alur_permohonan as a', 'a.id', 'vb.id_alur')
                ->where('a.upload', '<>', 4)
                ->where('b.id_permohonan', $request->input('id_permohonan'))
                ->whereNotNull('b.id_user_revisi')
                ->update(['vb.status_valid' => 0]);
        }
        $data->save();
        $permohonan = DB::table('permohonan as p')
                        ->select('p.*', 'u.email', 'u2.email as email_verifikator', 'u2.name as nama_verifikator')
                        ->join('users as u', 'p.id_user', 'u.id')
                        ->join('users as u2', 'u2.id', 'p.urutan_alur')
                        ->where('p.id', $request->id_permohonan)->first();
        $errors = array();
        if ($request->input('status') == 'Permohonan Baru') {
            try {
                // kirim email ke verifikator
                Mail::to($permohonan->email_verifikator)->send(new VerifikasiPermohonanBaruMail(collect($permohonan)));
                // kirim email ke pemohon
                Mail::to($permohonan->email)->send(new IzinDiajukan(collect($permohonan)));
            } catch (\Throwable $th) {
                array_push($errors, $th->getMessage());
            }

        } else if ($request->input('status') == 'Verifikasi Ulang') {
            try {
                Mail::to($permohonan->email_verifikator)->send(new VerifikasiUlangPermohonanMail(collect($permohonan)));
            } catch (\Throwable $th) {
                array_push($errors, $th->getMessage());
            }
        } else if ($request->input('status') == 'Revisi') {
            try {
                Mail::to($permohonan->email)->send(new RevisiSyaratBerkasMail(collect($permohonan)));
            } catch (\Throwable $th) {
                array_push($errors, $th->getMessage());
            }
        }

        return response()->json(['message' => 'Status permohonan berhasil diubah', 'data' => $data, 'mail_errors' => $errors], 201);
    }

    public function downloadTemplateWord(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_permohonan' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = Permohonan::where('id', $request->input('id_permohonan'))->with('jenisIzin')->with('kelengkapan')->with('detail')->first();
        $path = str_replace(env('APP_URL').'/', '', $data->jenisIzin->template_surat);
        $templateProcessor = new TemplateProcessor($path);
        $array_kode = array(
            'NAMA' => $data->nama,
            'NIK' => $data->nik,
            'NPWP' => $data->npwp,
            'TEMPAT_LAHIR' => $data->tempat_lahir,
            'NAMA_JNS_IZIN' => $data->nama_jenis_izin,
            'DESKRIPSI_JNS_IZIN' => $data->deskripsi_jenis_izin,
            'NO_REGISTRASI' => $data->no_registrasi
        );
        foreach ($data->detail as $key => $k) {
            $array_kode[$k->kode_isian] = $k->value;
        }
        foreach ($data->kelengkapan as $key => $k) {
            $array_kode[$k->kode_isian] = $k->value;
        }
        // dd($array_kode);
        $templateProcessor->setValues($array_kode);
        $filename = $data->id."_".$data->nama_jenis_izin."_".$data->nama.".docx";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $templateProcessor->saveAs('php://output');
    }

    public function uploadPDFTemplate(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'file' => 'required|mimes:pdf'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = Permohonan::find($id);
        if ($data) {
            if (!is_null($data->template_pdf)) {
                if(file_exists(str_replace(env('APP_URL'), ".", $data->template_pdf))) {
                    unlink(str_replace(env('APP_URL'), ".", $data->template_pdf));
                }
            }
            if ($file = $request->file('file')) {
                $path = $file->store('public/file_templatepermohonan');
                $data->template_pdf = env('APP_URL')."/storage/app/public/file_templatepermohonan/".$file->hashName();
                $data->save();
            }

            return response()->json(['message' => 'Template word berhasil dibuat/diupdate.', 'permohonan' => $data], 201);
        }
        else {
            return response()->json(['message' => 'Permohonan tidak ditemukan.'], 404);
        }
    }

    public function sendEmail()
    {
        // $data = array('nama'=>"Made Mas Adi Winata");

        // $details = [
        //     'title' => 'Mail from ItSolutionStuff.com',
        //     'body' => 'This is for testing email using smtp'
        // ];
        // Mail::to("m45adiwinata@gmail.com")->send(new MyTestMail($details));
        echo "Basic Email Sent. Check your inbox.";
    }

    public function emailIzinDiajukan(Request $request)
    {
        $data = DB::table('permohonan as p')
                    ->select('p.*', 'u.email')
                    ->join('users as u', 'p.id_user', 'u.id')
                    ->where('p.id', $request->id_permohonan)->first();
        try {
            Mail::to($data->email)->send(new IzinDiajukan(collect($data)));
        } catch (\Throwable $th) {
            return response()->json(['message' => 'Email izin gagal dikirim. '.$th->getMessage()], 200);
        }

        return response()->json(['message' => 'Email izin berhasil diajukan sudah dikirim'], 200);
    }

    public function deleteBerkas($id_berkas)
    {
        if (auth()->user()->id_role == 3) {
            return response()->json(['message' => 'Unauthorized access.'], 401);
        }
        BerkasPermohonan::where('id', $id_berkas)->delete();

        return response()->json(['message' => 'Berkas berhasil dihapus.'], 200);
    }

    public function getByJenisIzin(Request $request) {
        if (auth()->user()->id_role == 2) {
            $data = Permohonan::with('detail')->with('berkas')->with('alur')->with('kelengkapan');
            if (isset($request->id_jenis_izin)) {
                $data = $data->where('id_jenis_izin', $request->id_jenis_izin);
            }
            if (isset($request->limit)) {
                $data = $data->limit($request->limit);
                if (isset($request->pagenumber)) {
                    $data = $data->offset(($request->pagenumber - 1) * $request->limit);
                }
            }
            $data = $data->get();

            return response()->json(['data' => $data], 200);
        }
        return response()->json(['data' => [], 'message' => 'User tidak memiliki hak akses.'], 401);
    }

    public function laporanBulanan(Request $request) {
        $bulanTerkini = 12;
        $bulans = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );
        if (!isset($request->tahun)) {
            $request->tahun = date('Y');
            $bulanTerkini = (int)date('m');
        }
        else if ($request->tahun > date('Y')) {
            return response()->json(['message' => 'Harap masukkan tahun terkini/kebelakang.'], 400);
        }
        $sql_select = "";
        $sql_select2 = "";
        for ($i=1; $i <= $bulanTerkini; $i++) {
            $str = array_keys($bulans)[$i-1];
            $sql_select .= "IFNULL(pe.jml_{$str}, 0) AS jml_{$str},";
            $sql_select2 .= "SUM(IF(DATE_FORMAT(kp.value, '%m')='{$str}', 1, 0)) AS jml_{$str},";
        }

        $data = DB::table('jenis_izin as ji')
                ->selectRaw("ji.nama_jenis,
                    ".$sql_select."
                    IFNULL(pe.jml_total, 0) AS jml_total")
                ->leftJoin(DB::raw("(
                        SELECT p.id_jenis_izin,
                        ".$sql_select2."
                        COUNT(p.id) AS jml_total
                        FROM permohonan p
                        INNER JOIN kelengkapan_permohonan kp ON p.id = kp.id_permohonan AND (kp.`kode_isian`='TGL_SK' OR kp.`kode_isian` = 'TANGGAL_PENETAPAN')
                        WHERE p.`status`='Selesai'
                        AND YEAR(kp.value) = {$request->tahun}
                        AND p.deleted_at IS NULL
                        GROUP BY p.id_jenis_izin
                    ) pe"), 'pe.id_jenis_izin', 'ji.id')
                ->whereNull('ji.deleted_at')
                ->groupBy('ji.id')
                ->get();

        ob_start();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells("A1:R1");
        $sheet->setCellValue('A1', 'REKAPITULASI JUMLAH IJIN YANG TERBIT');
        $sheet->mergeCells("A2:R2");
        $sheet->setCellValue('A2', $request->tahun);
        $sheet->mergeCells("A3:A4");
        $sheet->setCellValue('A3', 'NO');
        $sheet->mergeCells("B3:E4");
        $sheet->setCellValue('B3', 'JENIS IJIN');
        $sheet->mergeCells("F3:Q3");
        $sheet->setCellValue('F3', 'DATA IJIN');
        $ordcol = ord('F');
        foreach ($bulans as $bulan) {
            $sheet->setCellValue(chr($ordcol).'4', $bulan);
            $ordcol++;
        }
        $sheet->mergeCells("R3:R4");
        $sheet->setCellValue('R3', 'TOTAL SEMUA IJIN');
        $rownum = 5;
        $jumlahs = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
        foreach ($data as $key => $d) {
            $sheet->setCellValue('A'.$rownum, $key+1);
            $sheet->mergeCells("B{$rownum}:E{$rownum}");
            $sheet->setCellValue('B'.$rownum, $d->nama_jenis);
            $ordcol = ord('F');
            foreach (array_keys($bulans) as $idx => $bulan_key) {
                if (isset($d->{'jml_'.$bulan_key})) {
                    $sheet->setCellValue(chr($ordcol).$rownum, $d->{'jml_'.$bulan_key});
                    $jumlahs[$idx] += $d->{'jml_'.$bulan_key};
                }
                $ordcol++;
            }
            $sheet->setCellValue('R'.$rownum, $d->jml_total);
            $jumlahs[12] += $d->jml_total;
            $rownum++;
        }
        $sheet->mergeCells("A{$rownum}:E{$rownum}");
        $sheet->setCellValue('A'.$rownum, 'JUMLAH');
        $ordcol = ord('F');
        foreach (array_keys($bulans) as $idx => $bulan_key) {
            if (isset($data[0]->{'jml_'.$bulan_key})) {
                $sheet->setCellValue(chr($ordcol).$rownum, $jumlahs[$idx]);
            }
            $ordcol++;
        }
        $sheet->setCellValue('R'.$rownum, $jumlahs[12]);

        $sheet->mergeCells('H'.($rownum+3).':K'.($rownum+3));
        $sheet->setCellValue('H'.($rownum+3), 'Kepala Dinas Penanaman Modal dan PTSP Kab. Buleleng');
        $sheet->mergeCells('H'.($rownum+7).':K'.($rownum+7));
        $sheet->setCellValue('H'.($rownum+7), 'I Made Kuta S.Sos');
        $sheet->getStyle('H'.($rownum+7))->getFont()->setUnderline(true);
        $sheet->mergeCells('H'.($rownum+8).':K'.($rownum+8));
        $sheet->setCellValue('H'.($rownum+8), 'NIP. 19700710 199203 1 007');
        $sheet->mergeCells('B'.($rownum+11).':E'.($rownum+11));
        $sheet->setCellValue('B'.($rownum+11), 'Analis Kebijakan Ahli Muda Pelayanan');
        $sheet->mergeCells('B'.($rownum+12).':E'.($rownum+12));
        $sheet->setCellValue('B'.($rownum+12), 'Perizinan dan Non Perizinan (B/I,II)');
        $sheet->mergeCells('B'.($rownum+16).':E'.($rownum+16));
        $sheet->setCellValue('B'.($rownum+16), 'Putu Gitarani Cahaya Putri Wijaya, ST');
        $sheet->getStyle('B'.($rownum+16))->getFont()->setUnderline(true);
        $sheet->mergeCells('B'.($rownum+17).':E'.($rownum+17));
        $sheet->setCellValue('B'.($rownum+17), 'NIP.199112022015032009');
        $sheet->mergeCells('N'.($rownum+11).':Q'.($rownum+11));
        $sheet->setCellValue('N'.($rownum+11), 'Analis Kebijakan Ahli Muda Pelayanan');
        $sheet->mergeCells('N'.($rownum+12).':Q'.($rownum+12));
        $sheet->setCellValue('N'.($rownum+12), 'Perizinan dan Non Perizinan (BIII)');
        $sheet->mergeCells('N'.($rownum+16).':Q'.($rownum+16));
        $sheet->setCellValue('N'.($rownum+16), 'Made Windu Segara Kurniawan, S.Kom');
        $sheet->getStyle('N'.($rownum+16))->getFont()->setUnderline(true);
        $sheet->mergeCells('N'.($rownum+17).':Q'.($rownum+17));
        $sheet->setCellValue('N'.($rownum+17), 'NIP. 197812312010011030');

        $tnr_bold = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '000000'),
                'size'  => 12,
                'name'  => 'Times New Roman'
            )
        );
        $tnr = array(
            'font'  => array(
                'color' => array('rgb' => '000000'),
                'size'  => 12,
                'name'  => 'Times New Roman'
            )
        );
        $tnr_bold10 = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Times New Roman'
            )
        );
        $tnr10 = array(
            'font'  => array(
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Times New Roman'
            )
        );
        $border = array(
            'borders' => [
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ],
        );
        $sheet->getColumnDimension('R')->setWidth(24);
        for ($i=ord('F'); $i <= ord('Q'); $i++) {
            $sheet->getColumnDimension(chr($i))->setWidth(12);
        }
        $sheet->getColumnDimension('B')->setWidth(20);

        $sheet->getStyle('A1:A2')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1:A2')->applyFromArray($tnr_bold);
        $sheet->getStyle('A3:R'.$rownum)->applyFromArray($border);
        $sheet->getStyle('A3:E4')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A3:E4')->applyFromArray($tnr_bold);
        $sheet->getStyle('F3:R3')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('F3:R3')->applyFromArray($tnr_bold);
        $sheet->getStyle('F4:Q4')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('F4:Q4')->applyFromArray($tnr);
        $sheet->getStyle('A5:B'.$rownum)->getAlignment()->setHorizontal('left');
        $sheet->getStyle('F5:R'.$rownum)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A5:R'.$rownum)->applyFromArray($tnr);
        $sheet->getStyle('A'.$rownum)->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A'.$rownum)->applyFromArray($tnr_bold);

        $sheet->getStyle('A'.($rownum+3).':R'.($rownum+17))->getAlignment()->setHorizontal('center');
        $sheet->getStyle('H'.($rownum+3).':K'.($rownum+8))->applyFromArray($tnr_bold10);
        $sheet->getStyle('B'.($rownum+11).':N'.($rownum+17))->applyFromArray($tnr10);

        $sheet->getStyle('B5:B'.$rownum)->getAlignment()->setWrapText(true);
        for ($i=5; $i <= $rownum; $i++) {
            $sheet->getRowDimension($i)->setRowHeight(
                15.5 * (substr_count($sheet->getCell('B'.$i)->getValue(), "\n") + 1)
            );
        }

        try {
            ob_clean();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Laporan Bulanan Per Tahun '.$request->tahun.'.xlsx"');
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
            exit(0);
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function uploadExtraImage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_permohonan' => 'required|integer',
            'image' => 'required|mimes:jpg|max:4096'
        ]);
        if($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = Permohonan::find($request->id_permohonan);
        if ($data) {
            if ($file = $request->file('image')) {
                $file->store('public/image_permohonan');
                $imgP = new PermohonanImages;
                $imgP->img_path = env('APP_URL')."/storage/app/public/image_permohonan/".$file->hashName();
                $data->images()->save($imgP);
                return response()->json([
                    "success" => true,
                    "message" => "Image successfully uploaded",
                    "data" => $data->with('images')->find($request->id_permohonan)
                ]);
            }
            return response()->json([
                "success" => false,
                "message" => "Image upload unsuccessful",
            ]);
        }
        return response()->json([
            "message" => "Permohonan tidak ditemukan.",
        ], 404);
    }

    private static function getQueries(Builder $builder)
    {
        $addSlashes = str_replace('?', "'?'", $builder->toSql());
        return vsprintf(str_replace('?', '%s', $addSlashes), $builder->getBindings());
    }
}
