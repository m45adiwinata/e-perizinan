<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JenisIzin;
use App\Models\DetailForm;
use App\Models\BerkasJenisIzin;
use App\Models\AlurJenisIzin;
use App\Models\KelengkapanIzin;
use Illuminate\Support\Facades\DB;
use Validator;

class JenisIzinController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        // $data = JenisIzin::get();
        $data = DB::table('jenis_izin');
        if (isset($request->name)) {
            $data = $data->where('nama_jenis', 'like', '%'.$request->name.'%');
        }
        $data = $data->whereNull('deleted_at')->get();

        return response()->json($data, 200);
    }

    public function view($id)
    {
        // return response()->json(["message" => "oke"], 200);
        $data = JenisIzin::with('detailForm')->with('berkas')->with('alur')->with('kelengkapan')->where('id', $id)->get();

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_jenis' => 'required|string',
            'deskripsi' => 'required',
            'detailForm' => 'required',
            'berkas' => 'required',
            'alur' => 'required',
            'kelengkapan' => 'required'
        ]);
        $data = new JenisIzin;
        $data->nama_jenis = $request->input('nama_jenis');
        $data->deskripsi = $request->input('deskripsi');
        $data->save();

        $data->detailForm()->createMany($request->input('detailForm'));
        $data->berkas()->createMany($request->input('berkas'));
        $data->alur()->createMany($request->input('alur'));
        $data->kelengkapan()->createMany($request->input('kelengkapan'));

        return response()->json($data->with('detailForm')->with('berkas')->with('alur')->with('kelengkapan')->where('id', $data->id)->get(), 201);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'id' => 'required',
            'nama_jenis' => 'required|string',
            'deskripsi' => 'required',
            'detailForm' => 'required',
            'berkas' => 'required',
            'alur' => 'required',
            'kelengkapan' => 'required'
        ]);
        $data = JenisIzin::find($request->input('id'));
        if (!$data) {
            return response()->json(['message' => 'Jenis izin tidak ditemukan.'], 404);
        }
        $data->nama_jenis = $request->input('nama_jenis');
        $data->deskripsi = $request->input('deskripsi');
        $data->save();
        
        $this->updateDetailForm($data, $request->input('detailForm'));
        $this->updateBerkas($data, $request->input('berkas'));
        $this->updateAlur($data, $request->input('alur'));
        $this->updateKelengkapan($data, $request->input('kelengkapan'));

        return response()->json($data->with('detailForm')->with('berkas')->with('alur')->with('kelengkapan')->where('id', $data->id)->get(), 201);
    }

    public function delete(Request $request)
    {
        $this->validate($request,[
            'id' => 'required'
        ]);
        $data = JenisIzin::find($request->input('id'));
        if ($data) {
            $data->delete();
            return response()->json(['message' => 'Data berhasil dihapus'], 201);
        }

        return response()->json(['message' => 'Data tidak ditemukan'], 404);
    }

    private function updateDetailForm($data, $updates)
    {
        $temp = array();
        foreach ($updates as $r) {
            if ($r['id'] == "0") {
                unset($r['id']);
                $r['id_jenis_izin'] = $data->id;
                $newItem = DetailForm::create($r);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                $e = DB::table('detail_form')->where('id_jenis_izin', $data->id)->where('id', $r['id'])->update(['label' => $r['label'], 'id_form_type' => $r['id_form_type'], 'kode_isian' => $r['kode_isian']]);
            }
        }
        DB::table('detail_form')->where('id_jenis_izin', $data->id)->whereNotIn('id', $temp)->delete();
    }

    private function updateBerkas($data, $updates)
    {
        $temp = array();
        foreach ($updates as $r) {
            if ($r['id'] == 0) {
                unset($r['id']);
                $r['id_jenis_izin'] = $data->id;
                $newItem = BerkasJenisIzin::create($r);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                $e = DB::table('berkas_jenis_izin')->where('id_jenis_izin', $data->id)->where('id', $r['id'])->update(['nama_berkas' => $r['nama_berkas'], 'required' => $r['required']]);
            }
        }
        DB::table('berkas_jenis_izin')->where('id_jenis_izin', $data->id)->whereNotIn('id', $temp)->delete();
    }

    private function updateAlur($data, $updates)
    {
        $temp = array();
        foreach ($updates as $r) {
            if ($r['id'] == 0) {
                unset($r['id']);
                $r['id_jenis_izin'] = $data->id;
                $newItem = AlurJenisIzin::create($r);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                $e = DB::table('alur')->where('id_jenis_izin', $data->id)->where('id', $r['id'])->update(['id_user' => $r['id_user'], 'nama' => $r['nama'], 'upload' => $r['upload'], 'no_urut' => $r['no_urut']]);
            }
        }
        DB::table('alur')->where('id_jenis_izin', $data->id)->whereNotIn('id', $temp)->delete();
    }

    private function updateKelengkapan($data, $updates)
    {
        $temp = array();
        foreach ($updates as $r) {
            if ($r['id'] == "0") {
                unset($r['id']);
                $r['id_jenis_izin'] = $data->id;
                $newItem = KelengkapanIzin::create($r);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                $e = DB::table('kelengkapan_izin')->where('id_jenis_izin', $data->id)->where('id', $r['id'])->update(['label' => $r['label'], 'id_form_type' => $r['id_form_type'], 'kode_isian' => $r['kode_isian']]);
            }
        }
        DB::table('kelengkapan_izin')->where('id_jenis_izin', $data->id)->whereNotIn('id', $temp)->delete();
    }

    public function uploadTemplate(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[ 
            'file' => 'required|mimes:doc,docx'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $jenis_izin = JenisIzin::find($id);
        if ($jenis_izin) {
            if (!is_null($jenis_izin->template_surat)) {
                if(file_exists(str_replace(env('APP_URL'), ".", $jenis_izin->template_surat))) {
                    unlink(str_replace(env('APP_URL'), ".", $jenis_izin->template_surat));
                }
            }
            if ($file = $request->file('file')) {
                $path = $file->store('public/file_templateword');
                $jenis_izin->template_surat = env('APP_URL')."/storage/app/public/file_templateword/".$file->hashName();
                $jenis_izin->save();
            }

            return response()->json(['message' => 'Template word berhasil dibuat/diupdate.', 'jenis_izin' => $jenis_izin], 201);
        }
        else {
            return response()->json(['message' => 'Jenis Izin tidak ditemukan.'], 404);
        }
    }
}
