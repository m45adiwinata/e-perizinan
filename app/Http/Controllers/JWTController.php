<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\EmailVerification;
use App\Mail\ResetPasswordMail;
use App\Mail\ResetPasswordDoneMail;

class JWTController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'verifyEmail', 'verifyUser', 'resetPasswordMail', 'resetPassword']]);
    }

    /**
     * Register user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:2|max:100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'jenis_kelamin' => 'required',
            'telepon' => 'required|string',
            'nik' => 'required|string',
            'alamat' => 'required|string',
            'id_role' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        
        $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'jenis_kelamin' => $request->jenis_kelamin,
                'telepon' => $request->telepon,
                'nik' => $request->nik,
                'alamat' => $request->alamat,
                'id_role' => $request->id_role
            ]);

        // $token = auth()->attempt(['email' => $request->email, 'password' => $request->password]);
        $errors = array();
        try {
            Mail::to($request->email)->send(new EmailVerification($user));
        } catch (\Throwable $th) {
            array_push($errors, $th->getMessage());
        }

        // return response()->json([
        //     'message' => 'User successfully registered',
        //     'user' => $user,
        //     'access_token' => $token
        // ], 201);
        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // return $this->respondWithToken($token);
        return response()->json([
            'message' => 'User logged in',
            'user' => auth()->user(),
            'access_token' => $token
        ], 201);
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'User successfully logged out.']);
    }

    /**
     * Refresh token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        return response()->json(auth()->user());
    }

    public function getUsers(Request $request)
    {
        $data = DB::table('users')
                    ->select('id', 'name', 'jenis_kelamin', 'telepon', 'nik', 'alamat', 'email', 'id_role');
        if($request->id_role) {
            $data = $data->whereIn('id_role', explode(',',$request->id_role));
        }
        if($request->name) {
            $data = $data->where('name', 'like', '%'.$request->name.'%');
        }
        $data = $data->get();
        if (count($data) > 0) {
            return response()->json($data, 200);
        }

        return response()->json(['message' => 'no data'], 404);
    }

    public function verifyEmail(Request $request)
    {
        $data = User::where('email', $request->email)->first();
        $errors = array();
        try {
            Mail::to($request->email)->send(new EmailVerification($data));
        } catch (\Throwable $th) {
            array_push($errors, $th->getMessage());
        }

        return response()->json(['message' => 'Email verifikasi sudah dikirim'], 200);
    }

    public function verifyUser($id)
    {
        User::where('id', $id)->update(['email_verified_at' => date("Y-m-d H:i:s")]);

        return response()->json(['message' => 'Email user berhasil diverifikasi', 'user' => User::find($id)], 200);
    }

    public function resetPasswordMail(Request $request)
    {
        $data = User::where('email', $request->email)->first();
        try {
            Mail::to($request->email)->send(new ResetPasswordMail($data));
        } catch (\Throwable $th) {
            array_push($errors, $th->getMessage());
        }

        return response()->json(['message' => 'Email reset password sudah dikirim'], 200);
    }

    public function resetPassword(Request $request)
    {
        User::where('email', $request->email)->update(['password' => Hash::make('S!aja!b1234')]);
        $user = User::where('email', $request->email)->first();
        try {
            Mail::to($request->email)->send(new ResetPasswordDoneMail($user));
        } catch (\Throwable $th) {
            array_push($errors, $th->getMessage());
        }

        return response()->json(['message' => 'Password user berhasil direset', 'user' => $user], 200);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}