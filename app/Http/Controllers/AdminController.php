<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Permohonan;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function rollbackAlurPermohonan(Request $request)
    {
        if (auth()->user()->id_role != 1) {
            return response()->json(['message'=>'Akses gagal.'], 401);
        }

        $validator = Validator::make($request->all(),[
            'id_permohonan' => 'required',
            'id_alur' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        $permohonan = Permohonan::find($request->input('id_permohonan'));
        if ($permohonan) {
            $alur = DB::table('alur_permohonan')->where('id_permohonan', $request->input('id_permohonan'))
                    ->where('id', $request->input('id_alur'))->first();
            DB::table('alur_permohonan')
                ->where('id_permohonan', $request->input('id_permohonan'))
                ->where('no_urut', '>=', $alur->no_urut)
                ->update(['status' => 0]);
            DB::table('berkas_permohonan as bp')
                ->leftJoin('alur_permohonan as ap', function($join) {
                    $join->on('ap.id_permohonan', 'bp.id_permohonan');
                    $join->on('ap.id_user', 'bp.id_user_revisi');
                })
                ->where('bp.id_permohonan', $request->input('id_permohonan'))
                ->where('ap.no_urut', '>=', $alur->no_urut)
                ->update(['bp.id_user_revisi' => null, 'bp.ket_revisi' => null]);
            DB::table('detail_permohonan as dp')
                ->leftJoin('alur_permohonan as ap', function($join) {
                    $join->on('ap.id_permohonan', 'dp.id_permohonan');
                    $join->on('ap.id_user', 'dp.id_user_revisi');
                })
                ->where('dp.id_permohonan', $request->input('id_permohonan'))
                ->where('ap.no_urut', '>=', $alur->no_urut)
                ->update(['dp.id_user_revisi' => null, 'dp.ket_revisi' => null]);
            DB::table('validasi_berkas as vb')
                ->join('alur_permohonan as ap', 'ap.id', 'vb.id_alur')
                ->join('permohonan as p', 'p.id', 'ap.id_permohonan')
                ->where('p.id', $permohonan->id)
                ->where('ap.no_urut', '>=', $alur->no_urut)
                ->update(['status_valid' => 0]);

            $permohonan->urutan_alur = $alur->id_user;
            $permohonan->status = 'Permohonan Baru';
            $permohonan->save();
            return response()->json(['message'=>'Permohonan berhasil dirollback.'], 201);
        } else {
            return response()->json(['message'=>'Permohonan tidak ditemukan.'], 404);
        }
    }

    public function resetPermohonan(Request $request)
    {
        if (auth()->user()->id_role != 1) {
            return response()->json(['message'=>'Akses gagal.'], 401);
        }

        $validator = Validator::make($request->all(),[
            'id_permohonan' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        $permohonan = Permohonan::find($request->input('id_permohonan'));
        if ($permohonan) {
            $alur = DB::table('alur_permohonan')->where('id_permohonan', $request->input('id_permohonan'))
                    ->where('no_urut', 1)->first();
            DB::table('alur_permohonan')
                ->where('id_permohonan', $request->input('id_permohonan'))
                ->update(['status' => 0]);
                DB::table('berkas_permohonan as bp')
                ->leftJoin('alur_permohonan as ap', function($join) {
                    $join->on('ap.id_permohonan', 'bp.id_permohonan');
                    $join->on('ap.id_user', 'bp.id_user_revisi');
                })
                ->where('bp.id_permohonan', $request->input('id_permohonan'))
                ->update(['bp.id_user_revisi' => null, 'bp.ket_revisi' => null]);
            DB::table('detail_permohonan as dp')
                ->leftJoin('alur_permohonan as ap', function($join) {
                    $join->on('ap.id_permohonan', 'dp.id_permohonan');
                    $join->on('ap.id_user', 'dp.id_user_revisi');
                })
                ->where('dp.id_permohonan', $request->input('id_permohonan'))
                ->update(['dp.id_user_revisi' => null, 'dp.ket_revisi' => null]);
            DB::table('validasi_berkas as vb')
                ->join('alur_permohonan as ap', 'ap.id', 'vb.id_alur')
                ->join('permohonan as p', 'p.id', 'ap.id_permohonan')
                ->where('p.id', $permohonan->id)
                ->update(['status_valid' => 0]);

            $permohonan->urutan_alur = $alur->id_user;
            $permohonan->status = 'Permohonan Baru';
            $permohonan->save();
            return response()->json(['message'=>'Permohonan berhasil direset.'], 201);
        } else {
            return response()->json(['message'=>'Permohonan tidak ditemukan.'], 404);
        }
    }
}
