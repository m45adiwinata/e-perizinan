<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetailForm;
use Illuminate\Support\Facades\DB;

class DetailFormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['getFormType']]);
    }

    public function index()
    {
        return response()->json(DetailForm::get(), 200);
    }

    public function view($id_jenis_izin)
    {
        $data = DetailForm::where('id_jenis_izin', $id_jenis_izin)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'id_jenis_izin' => 'required|string',
            'label' => 'required|string',
            'id_form_type' => 'required',
        ]);
        $data = new DetailForm;
        $data->id_jenis_izin = $request->input('id_jenis_izin');
        $data->label = $request->input('label');
        $data->id_form_type = $request->input('id_form_type');
        $data->save();

        return response()->json(['message' => 'Detail form berhasil disimpan.', 'data' => $data], 201);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'id' => 'required',
            'id_jenis_izin' => 'required|string',
            'label' => 'required|string',
            'id_form_type' => 'required',
        ]);
        $data = DetailForm::find($request->input('id'));
        $data->id_jenis_izin = $request->input('id_jenis_izin');
        $data->label = $request->input('label');
        $data->id_form_type = $request->input('id_form_type');
        $data->save();

        return response()->json(['message' => 'Detail form berhasil diupdate.', 'data' => $data], 201);
    }

    public function getFormType()
    {
        return response()->json(DB::table('form_type')->get(), 200);
    }
}
