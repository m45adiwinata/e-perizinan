<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\RegisterReklame;
use App\Models\Permohonan;
use App\Models\AlurPermohonan;
use App\Models\ValidasiBerkas;
use App\Models\ReklamePembayaran;
use App\Models\DetailPermohonan;
use Mail;
use App\Mail\UploadSKPDMail;
class ReklameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    private function collectionParse($col)
    {
        $col = $col->map(function ($value, $key) {
            return (array) $value;
        });

        return $col;
    }

    private function updateDetailPermohonan($data, $updates)
    {
        $temp = array();
        foreach ($updates as $r) {
            if ($r['id'] == 0) {
                unset($r['id']);
                $r['id_permohonan'] = $data->id;
                $newItem = DetailPermohonan::create($r);
                array_push($temp, $newItem->id);
            }
            else {
                array_push($temp, $r['id']);
                DB::table('detail_permohonan')->where('id_permohonan', $data->id)->where('id', $r['id'])->update($r);
            }
        }
        DB::table('detail_permohonan')->where('id_permohonan', $data->id)->whereNotIn('id', $temp)->delete();
    }

    private function getArrIdPermohonan($no_reg)
    {
        $permohonans = DB::table('permohonan')
                            ->select('id')
                            ->whereNull('deleted_at')
                            ->where('no_registrasi', $no_reg)
                            ->get();
        if (count($permohonans) == 0) {
            return response()->json(['message'=> "Permohonan tidak ditemukan dengan nomor registrasi ".$no_reg."."], 404);
        }
        $arrIdPermohonan = array_map(function ($p) {
                    return $p->id;
                }, $permohonans->toArray());
        return $arrIdPermohonan;
    }

    public function index(Request $request) 
    {
        $data = RegisterReklame::with(['reklame' => function($q) {
            $q->with('alur')->with('detail')->with('berkas')->with('kelengkapan')->get();
        }]);
        if (isset($request->tanggal)) {
            if (isset($request->tanggal_akhir)) {
                $data = $data->whereDate('created_at', '>=', $request->tanggal)->whereDate('created_at', '<=', $request->tanggal_akhir);
                $cnt = RegisterReklame::whereDate('created_at', '>=', $request->tanggal)->whereDate('created_at', '<=', $request->tanggal_akhir)->count();
            } else {
                $data = $data->whereDate('created_at', '=', $request->tanggal);
                $cnt = RegisterReklame::whereDate('created_at', '=', $request->tanggal)->count();
            }
        } else {
            $cnt = RegisterReklame::count();
        }
        if (isset($request->limit)) {
            $data = $data->limit($request->limit);
            if (isset($request->pagenumber)) {
                $data = $data->offset(($request->pagenumber - 1) * $request->limit);
            }
        }
        $data = $data->get();
        // dd($data);
        return response()->json(['count' => $cnt,'data'=>$data], 200);
    }

    public function view($id)
    {
        $data = RegisterReklame::with(['permohonan' => function($q) {
            $q->with('alur')->with('detail')->with('berkas')->with('kelengkapan')->with('reklamePembayaran')->get();
        }])->where('id', $id)->first();

        return response()->json(['data'=>$data], 200);
    }

    public function save(Request $request) 
    {
        // $validator = Validator::make($request->all(),[ 
        //     'nama_reg' => 'required',
        //     'nik_reg' => 'required',
        //     'npwp_reg' => 'required',
        //     'nama_perusahaan' => 'required',
        //     'alamat_perusahaan' => 'required',
        //     'no_telp' => 'required'
        // ]);
        // if($validator->fails()) {          
        //     return response()->json(['error'=>$validator->errors()], 401);                        
        // }
        // $new_data = $validator->validated();
        $new_data = $request->all();
        $new_data['no_reg'] = "BLL/".time()."/9"."/".auth()->user()->id;
        $new_data['id_user'] = auth()->user()->id;
        $data_reg = RegisterReklame::create($new_data);

        return response()->json(['message' => 'Berhasil membuat registrasi reklame','data'=>$data_reg], 201);
    }

    public function saveFromSiAjaib($request)
    {
        RegisterReklame::create($request);

    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[ 
            // 'nama_reg' => 'required',
            // 'nik_reg' => 'required',
            // 'npwp_reg' => 'required',
            // 'nama_perusahaan' => 'required',
            // 'alamat_perusahaan' => 'required',
            // 'no_telp' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        $data_reg = RegisterReklame::where('id', $id)->update([
            'nama_reg' => isset($request->nama_reg) ? $request->nama_reg : null,
            'nik_reg' => isset($request->nik_reg) ? $request->nik_reg : null,
            'npwp_reg' => isset($request->npwp_reg) ? $request->npwp_reg : null,
            'nama_perusahaan' => isset($request->nama_perusahaan) ? $request->nama_perusahaan : null,
            'alamat_perusahaan' => isset($request->alamat_perusahaan) ? $request->alamat_perusahaan : null,
            'no_telp' => isset($request->no_telp) ? $request->no_telp : null
        ]);
        $data_reg = RegisterReklame::find($id);
        if ($data_reg and !is_null($data_reg->no_reg)) {
            $data_reklame = Permohonan::where('no_registrasi', $data_reg->no_reg)->update([
                'nama' => $data_reg->nama_reg,
                'nik' => $data_reg->nik_reg,
                'npwp' => $data_reg->npwp_reg
            ]);
        }

        return response()->json([
            'message' => 'Berhasil mengupdate registrasi reklame',
            'data' => $data_reg
        ], 201);
    }

    public function delete($id)
    {
        $register = RegisterReklame::find($id);
        Permohonan::where('no_registrasi', $register->no_reg)->delete();
        $register->delete();

        return response()->json([
            'message' => 'Berhasil menghapus registrasi reklame'
        ], 201);
    }

    public function saveReklame(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id_reg' => 'required',
            'detailForm' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        
        $jenis_izin = DB::table('jenis_izin')->where('id', 9)->first();
        $kelengkapan = DB::table('kelengkapan_izin')
                        ->select('label', 'id_form_type', 'kode_isian')
                        ->where('id_jenis_izin', 9)->get();
        $kelengkapan = $this->collectionParse($kelengkapan);
        // dd($kelengkapan);
        $alur = DB::table('alur')
                ->select('id_user', 'nama', 'upload', 'no_urut')
                ->where('id_jenis_izin', 9)
                ->get();
        $alur = $this->collectionParse($alur);
        $regis = RegisterReklame::find($request->input('id_reg'));
        $detailForm = $request->input('detailForm');
        array_push($detailForm, array(
            'label' => 'Alamat',
            'form_type' => 1,
            'kode_isian' => 'ALAMAT',
            'value' => $regis->alamat_perusahaan
        ), array(
            'label' => 'Nama Perusahaan',
            'form_type' => 1,
            'kode_isian' => 'NAMA_PERUSAHAAN',
            'value' => $regis->nama_perusahaan
        ), array(
            'label' => 'No. Telp/HP',
            'form_type' => 1,
            'kode_isian' => 'HP/TELP',
            'value' => $regis->no_telp
        ));

        $permohonan = new Permohonan;
        $permohonan->id_user = $regis->id_user;
        $permohonan->id_jenis_izin = 9;
        $permohonan->nama_jenis_izin = $jenis_izin->nama_jenis;
        $permohonan->deskripsi_jenis_izin = $jenis_izin->deskripsi;
        $permohonan->no_registrasi = $regis->no_reg;
        $permohonan->memohon_untuk = 1;
        $permohonan->nama = $regis->nama_reg;
        $permohonan->nik = $regis->nik_reg;
        $permohonan->npwp = $regis->npwp_reg;
        $permohonan->tempat_lahir = "-";
        $permohonan->urutan_alur = $alur[0]['id_user'];
        $permohonan->save();

        $permohonan->detail()->createMany($detailForm);
        $permohonan->alur()->createMany($alur);
        if ($permohonan->alur()->where('upload', 1)->first()) {
            $permohonan->alur()->where('no_urut', 1)->update(['up_sp_rekomendasi' => 1]);
        }
        $last_alur = $permohonan->alur()->orderBy('no_urut', 'desc')->first();
        $last_alur->as_penandatangan = 1;
        $last_alur->save();
        $permohonan->kelengkapan()->createMany($kelengkapan);
        $permohonan->reklamePembayaran()->create(['statuslunas' => 0]);

        return response()->json([
            'message' => 'Data reklame tersimpan',
            'data' => $permohonan
                ->with('detail')
                ->with('alur')
                ->with('kelengkapan')
                ->where('id', $permohonan->id)->get()], 201);
    }

    public function updateReklame(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[ 
            'detailForm' => 'required',
            'alasan' => 'required',
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }

        $permohonan = Permohonan::where('id_jenis_izin', 9)->find($id);
        if (is_null($permohonan)) {
            return response()->json(['message' => 'Reklame tidak ditemukan'], 404);
        }
        $this->updateDetailPermohonan($permohonan, $request->input('detailForm'));
        $permohonan->save();

        return response()->json([
            'message' => 'Data reklame berhasil diubah',
            'data' => $permohonan
                ->with('detail')
                ->with('alur')
                ->with('kelengkapan')
                ->where('id', $permohonan->id)->get()], 201);
    }

    public function deleteReklame($id)
    {
        Permohonan::where('id', $id)->delete();

        return response()->json([
            'message' => 'Data reklame berhasil dihapus',
            ], 201);
    }

    public function uploadBerkasPerRegistrasi(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'isreplace' => 'required',
            'no_reg' => 'required',
            'nama_berkas' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        
        $arrIdPermohonan = $this->getArrIdPermohonan($request->input('no_reg'));
        if ($request->input('isreplace') != 0) {
            $data = DB::table('berkas_permohonan')
                        ->whereIn('id_permohonan', $arrIdPermohonan)
                        ->where('nama_berkas', $request->input('nama_berkas'))->get();
            if(count($data) > 0) {
                if(file_exists(str_replace(env('APP_URL'), ".", $data[0]->file))) {
                    unlink(str_replace(env('APP_URL'), ".", $data[0]->file));
                }
                DB::table('berkas_permohonan')
                        ->whereIn('id_permohonan', $arrIdPermohonan)
                        ->where('nama_berkas', $request->input('nama_berkas'))
                        ->update(['file' => null]);
            } else {
                return response()->json(['message'=> "Gagal menemukan berkas dengan nama file ".$request->input('nama_berkas')."."], 404);
            }
        }
        
        $berkas_izin = DB::table('berkas_jenis_izin')
                            ->where('id_jenis_izin', 9)
                            ->where('nama_berkas', $request->input('nama_berkas'))
                            ->first();
        if (is_null($berkas_izin)) {
            return response()->json(['message'=> "Nama berkas yang dikirim tidak termasuk dalam berkas yang diperlukan untuk jenis izin terkait."], 404);
        }
        if ($file = $request->file('file')) {
            if ($file->getClientOriginalExtension() != 'pdf') {
                return response()->json(['message'=> "Berkas yang diupload harus format pdf."], 406);
            }
            if (!$file->getSize() || $file->getSize() > 4*1000000) {
                return response()->json(['message'=> "Batas ukuran maksimal berkas 2mb"], 406);
            }
            $path = $file->store('public/file_berkaspermohonan');
            $new_bp = DB::table('berkas_permohonan')
                            ->whereIn('id_permohonan', $arrIdPermohonan)
                            ->where('nama_berkas', $request->input('nama_berkas'))
                            ->get();
            if (count($new_bp) == 0) {
                $new_berkases = array();
                foreach ($arrIdPermohonan as $p) {
                    array_push($new_berkases, [
                        'id_permohonan' => $p,
                        'nama_berkas' => $request->input('nama_berkas'),
                        'file' => env('APP_URL')."/storage/app/public/file_berkaspermohonan/".$file->hashName(),
                        'required' => $berkas_izin ? $berkas_izin->required : 0
                    ]);
                }
                $new_bp = DB::table('berkas_permohonan')->insert($new_berkases);
            } else {
                DB::table('berkas_permohonan')
                        ->whereIn('id_permohonan', $arrIdPermohonan)
                        ->where('nama_berkas', $request->input('nama_berkas'))
                        ->update(['file' => env('APP_URL')."/storage/app/public/file_berkaspermohonan/".$file->hashName()]);
            }
            $berkas = DB::table('berkas_permohonan')
                            ->whereIn('id_permohonan', $arrIdPermohonan)
                            ->where('nama_berkas', $request->input('nama_berkas'))
                            ->get();
            
            foreach (AlurPermohonan::whereIn('id_permohonan', $arrIdPermohonan)->get(['id', 'upload']) as $a) {
                foreach ($berkas as $b) {
                    if ($a->upload == 4) {
                        $init_status_valid = 1;
                    }
                    else {
                        $init_status_valid = 0;
                    }
                    ValidasiBerkas::updateOrCreate(
                        ['id_alur' => $a->id, 'id_berkas' => $b->id],
                        ['id_alur' => $a->id, 'id_berkas' => $b->id, 'status_valid' => $init_status_valid]
                    );
                }
            }
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "data" => Permohonan::with('berkas')->where('no_registrasi', $request->input('no_reg'))->get()
            ]);
        }

        return response()->json([
            "success" => true,
            "message" => "Berkas permohonan telah dihapus.",
            "data" => Permohonan::with('berkas')->where('no_registrasi', $request->input('no_reg'))->get()
        ]);
    }

    public function isiKelengkapanPerRegistrasi(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'no_reg' => 'required',
            'kelengkapan' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        
        $arrIdPermohonan = $this->getArrIdPermohonan($request->input('no_reg'));
        DB::transaction(function () use ($request, $arrIdPermohonan) {
            foreach ($request->input('kelengkapan') as $key => $k) {
                DB::table('kelengkapan_permohonan')
                        ->whereIn('id_permohonan', $arrIdPermohonan)
                        ->where('label', $k['label'])
                        ->update(['value' => $k['value']]);
            }
        });

        return response()->json([
            'message'=>'Kelengkapan per nomor registrasi berhasil diisi',
            'data' => Permohonan::with('kelengkapan')
                        ->where('permohonan.no_registrasi', $request->input('no_reg'))
                        ->get()
        ], 201); 
    }

    public function isiKelengkapanPerReklame(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id_reklame' => 'required',
            'kelengkapan' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        
        DB::transaction(function () use ($request) {
            foreach ($request->input('kelengkapan') as $k) {
                DB::table('kelengkapan_permohonan')
                        ->where('id_permohonan', $request->input('id_reklame'))
                        ->where('label', $k['label'])
                        ->update(['value' => $k['value']]);
            }
        });

        return response()->json([
            'message'=>'Kelengkapan per reklame berhasil diisi',
            'data' => Permohonan::with('kelengkapan')
                        ->find($request->input('id_reklame'))
        ], 201); 
    }

    public function uploadSKPD(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id_permohonan' => 'required',
            'no_skpd' => 'required|string',
            'file' => 'required|mimes:pdf|max:4096',
            'nominal' => 'required|integer'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = ReklamePembayaran::where('id_permohonan', $request->input('id_permohonan'))->first();
        if (!$data) {
            return response()->json(['message'=>'Pembayaran reklame tidak ada.'], 404);
        }
        if ($file = $request->file('file')) {
            if (!is_null($data->fileskpd)) {
                if(file_exists(str_replace(env('APP_URL'), ".", $data->fileskpd))) {
                    unlink(str_replace(env('APP_URL'), ".", $data->fileskpd));
                }
            }
            $path = $file->store('public/file_skpdreklame');
            DB::table('reklame_pembayaran')
                ->where('id_permohonan', $request->input('id_permohonan'))
                ->update([
                    'fileskpd' => env('APP_URL')."/storage/app/public/file_skpdreklame/".$file->hashName(), 
                    'nominal' => $request->input('nominal'),
                    'no_skpd' => $request->input('no_skpd')
                ]);
            $data = Permohonan::with('reklamePembayaran')->find($request->input('id_permohonan'));
            $pemohon = DB::table('users')->find($data->id_user);
            $errors = array();
            try {
                Mail::to($pemohon->email)->send(new UploadSKPDMail(collect($data)));
            } catch (\Throwable $th) {
                array_push($errors, $th->getMessage());
            }

            return response()->json([
                'message'=>'Upload bukti pembayaran reklame berhasil.',
                'data' => $data,
                'errors' => $errors
            ], 201);
        }

        return response()->json(['message'=>'no file uploaded found.'], 404);
    }

    public function countByStatus()
    {
        $data = DB::select(DB::raw("SELECT 'belum' AS `status`, COUNT(id) AS jumlah FROM permohonan WHERE id_jenis_izin = 9 AND deleted_at IS NULL AND `status`='' UNION ALL
                                    SELECT 'proses' AS `status`, COUNT(id) AS jumlah FROM permohonan WHERE id_jenis_izin = 9 AND deleted_at IS NULL AND `status` != 'Selesai' AND `status` != '' UNION ALL
                                    SELECT 'sudah' AS `status`, COUNT(id) AS jumlah FROM permohonan WHERE id_jenis_izin = 9 AND deleted_at IS NULL AND `status` = 'Selesai'"));
        
        return response()->json($data, 200);
    }

    public function listReklame(Request $request)
    {
        $data = Permohonan::join('detail_permohonan as dp', 'dp.id_permohonan', '=', 'permohonan.id')
                    ->join('detail_permohonan as dpx', 'dpx.id_permohonan', '=', 'permohonan.id')
                    ->join('detail_permohonan as dpy', 'dpy.id_permohonan', '=', 'permohonan.id')
                    ->join('detail_permohonan as dpz', 'dpz.id_permohonan', '=', 'permohonan.id')
                    ->join('registrasi_reklame as rr', 'rr.no_reg', '=', 'permohonan.no_registrasi')
                    ->where('id_jenis_izin', 9)
                    ->where('dp.label', 'Tanggal Akhir Pemasangan')
                    ->where('dpx.label', 'Tempat Pemasangan')
                    ->where('dpy.label', 'Jenis Reklame')
                    ->where('dpz.label', 'Titik Koordinat Pemasangan')
                    ->selectRaw(
                        'permohonan.id,
                        rr.id as id_registrasi,
                        permohonan.id_user,
                        permohonan.nama_jenis_izin,
                        permohonan.deskripsi_jenis_izin,
                        permohonan.no_registrasi,
                        permohonan.memohon_untuk,
                        permohonan.nama,
                        permohonan.nik,
                        permohonan.npwp,
                        permohonan.tempat_lahir,
                        permohonan.urutan_alur,
                        IF(permohonan.status != "", IF(permohonan.status = "Selesai", "sudah", "proses"), "belum") as status,
                        rr.id as id_register,
                        rr.nama_perusahaan, 
                        dp.value as tgl_akhir, 
                        dpx.value as tempat_pemasangan, 
                        dpy.value as jenis_reklame,
                        dpz.value as titik_koordinat,
                        permohonan.created_at');
        
        
        if (isset($request->jatuh_tempo)) {
            if ($request->jatuh_tempo == 'bulan ini') {
                $data = $data->where(DB::raw('DATE_FORMAT(dp.value, "%Y%m")'), date('Ym'));
            } else {
                $data = $data->where(DB::raw('DATE_FORMAT(dp.value, "%Y%m")'), date('Ym', strtotime("+1 month", strtotime(date('Y-m-d')))));
            }
        }
        if (isset($request->nama_perusahaan)) {
            $data = $data->where('rr.nama_perusahaan', 'like', '%'.$request->nama_perusahaan.'%');
        }
        if (isset($request->no_reg)) {
            $data = $data->where('permohonan.no_registrasi', 'like', '%'.$request->no_reg.'%');
        }
        if (isset($request->status)) {
            if ($request->status == 'belum') {
                $data = $data->where('permohonan.status', '');
            } else if ($request->status == 'sudah') {
                $data = $data->where('permohonan.status', 'Selesai');
            } else if ($request->status == 'proses') {
                $data = $data->whereNotIn('permohonan.status', ['', 'Selesai']);
            }
        }
        $count = count($data->get());

        if (isset($request->sort)) {
            if (isset($request->order)) {
                $data = $data->orderBy($request->sort, $request->order);
            } else {
                $data = $data->orderBy('tgl_akhir');
            }
        }
        if (isset($request->limit)) {
            $data = $data->limit($request->limit);
            if (isset($request->pagenumber)) {
                $data = $data->offset(($request->pagenumber - 1) * $request->limit);
            }
        }
        $data = $data->get();

        return response()->json(['count'=> $count, 'data'=>$data], 200);
    }

    public function listReklameExpired(Request $request)
    {
        $data = Permohonan::join('detail_permohonan as dp', 'dp.id_permohonan', '=', 'permohonan.id')
                    ->join('detail_permohonan as dpx', 'dpx.id_permohonan', '=', 'permohonan.id')
                    ->join('detail_permohonan as dpy', 'dpy.id_permohonan', '=', 'permohonan.id')
                    ->join('registrasi_reklame as rr', 'rr.no_reg', '=', 'permohonan.no_registrasi')
                    ->where('id_jenis_izin', 9)
                    ->where('dp.label', 'Tanggal Akhir Pemasangan')
                    ->where('dpx.label', 'Tempat Pemasangan')
                    ->where('dpy.label', 'Jenis Reklame')
                    ->where('dp.value', '<=', date('Y-m-d'))
                    ->selectRaw(
                        'permohonan.id,
                        rr.id as id_registrasi,
                        permohonan.id_user,
                        permohonan.nama_jenis_izin,
                        permohonan.deskripsi_jenis_izin,
                        permohonan.no_registrasi,
                        permohonan.memohon_untuk,
                        permohonan.nama,
                        permohonan.nik,
                        permohonan.npwp,
                        permohonan.tempat_lahir,
                        permohonan.urutan_alur,
                        IF(permohonan.status != "", IF(permohonan.status = "Selesai", "sudah", "proses"), "belum") as status,
                        rr.id as id_register,
                        rr.nama_perusahaan, 
                        dp.value as tgl_akhir, 
                        dpx.value as tempat_pemasangan, 
                        dpy.value as jenis_reklame'
                    );
        
        
        if (isset($request->jatuh_tempo)) {
            if ($request->jatuh_tempo == 'bulan ini') {
                $data = $data->where(DB::raw('DATE_FORMAT(dp.value, "%Y%m")'), date('Ym'));
            } else {
                $data = $data->where(DB::raw('DATE_FORMAT(dp.value, "%Y%m")'), date('Ym', strtotime("+1 month", strtotime(date('Y-m-d')))));
            }
        }
        if (isset($request->tanggal)) {
            if (isset($request->tanggal_akhir)) {
                $data = $data->whereDate('dp.value', '>=', $request->tanggal)->whereDate('dp.value', '<=', $request->tanggal_akhir);
            } else {
                $data = $data->whereDate('dp.value', '=', $request->tanggal);
            }
        }
        if (isset($request->nama_perusahaan)) {
            $data = $data->where('rr.nama_perusahaan', 'like', '%'.$request->nama_perusahaan.'%');
        }
        if (isset($request->status)) {
            if ($request->status == 'belum') {
                $data = $data->where('permohonan.status', '');
            } else if ($request->status == 'sudah') {
                $data = $data->where('permohonan.status', 'Selesai');
            } else if ($request->status == 'proses') {
                $data = $data->whereNotIn('permohonan.status', ['', 'Selesai']);
            }
        }
        $count = count($data->get());
        if (isset($request->sort)) {
            if (isset($request->order)) {
                $data = $data->orderBy($request->sort, $request->order);
            } else {
                $data = $data->orderBy('tgl_akhir');
            }
        }
        if (isset($request->limit)) {
            $data = $data->limit($request->limit);
            if (isset($request->pagenumber)) {
                $data = $data->offset(($request->pagenumber - 1) * $request->limit);
            }
        }
        $data = $data->get();

        return response()->json(['count'=> $count, 'data'=>$data], 200);
    }

    public function listReklamePosition()
    {
        $data = Permohonan::join('registrasi_reklame as rr', 'rr.no_reg', '=', 'permohonan.no_registrasi')
                    ->join('detail_permohonan as dp', 'dp.id_permohonan', '=', 'permohonan.id')
                    ->where('id_jenis_izin', 9)
                    ->where ('dp.label', 'Titik Koordinat Pemasangan')
                    ->select('permohonan.id as id', 'rr.nama_perusahaan as nama', 'permohonan.status as status', 'dp.value as koordinat')
                    ->get()->toArray();

        $pos = array_map(function($d) {
            if ($d['status'] == 'Selesai') {
                $status = 'sudah';
            } else if ($d['status'] == '') {
                $status = 'belum';
            } else {
                $status = 'proses';
            }
            $expl = explode(',', $d['koordinat']);
            $temp = array(
                'id' => $d['id'],
                'name' => $d['nama'],
                'status' => $status,
                'position' => array(
                    'lat' => $expl[0],
                    'lng' => isset($expl[1]) ? $expl[1] : '-'
                )
            );
            return $temp;
        }, $data);

        return response()->json(['data'=>$pos], 200);
    }

    public function claimRegister(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'no_reg' => 'required|string'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = Permohonan::join('reklame_pembayaran as rp', 'rp.id_permohonan', 'permohonan.id')
                            ->where('no_registrasi', $request->input('no_reg'));
        $cekStatus = $data->select('permohonan.status')->get();
        if (is_null($cekStatus)) {
            return response()->json([
                'message' => 'permohonan reklame dengan nomor registrasi tersebut tidak ditemukan'
            ], 404);
        } else {
            $claimed = false;
            foreach ($cekStatus as $c) {
                if ($c->status != "") {
                    $claimed = true;
                }
            }
            if ($claimed) {
                return response()->json([
                    'message' => 'permohonan sudah diclaim. harap hubungi admin'
                ], 401);
            }
        }
        $data->update(['id_user' => auth()->user()->id]);
        $cnt = $data->count();
        $bayar = $data->sum('rp.nominal');
        $data = $data->select('permohonan.*', 'rp.nominal', 'rp.fileskpd')->get();
        $register = RegisterReklame::where('no_reg', $request->input('no_reg'))->first();

        return response()->json([
            'message' => 'berhasil mengklaim registrasi', 
            'count' => $cnt,
            'data' => $data, 
            'register' => $register,
            'totalbayar' => $bayar
        ], 201);
    }

    public function perpanjangReklame(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id_reklame' => 'required|integer',
            'tanggal_mulai' => 'required',
            'tanggal_akhir' => 'required'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = Permohonan::with('berkas')->find($request->input('id_reklame'));
        $no_reg = $data->no_registrasi;
        $arrIdPermohonan = $this->getArrIdPermohonan($no_reg);
        $success = true;
        try {
            DB::transaction(function () use ($request, $arrIdPermohonan) {
                DB::table('detail_permohonan')
                    ->where('id_permohonan', $request->input('id_reklame'))
                    ->where('label', 'Tanggal Mulai Pemasangan')
                    ->update(['value' => $request->input('tanggal_mulai')]);
                DB::table('detail_permohonan')
                    ->where('id_permohonan', $request->input('id_reklame'))
                    ->where('label', 'Tanggal Akhir Pemasangan')
                    ->update(['value' => $request->input('tanggal_akhir')]);
                DB::table('berkas_permohonan')
                    ->whereIn('id_permohonan', $arrIdPermohonan)
                    ->update(['file' => null]);
            });
        } catch (\Throwable $th) {
            $success = false;
        }
        
        if ($success) {
            foreach ($data->berkas as $b) {
                if (!is_null($b->file)) {
                    if(file_exists(str_replace(env('APP_URL'), ".", $b->file))) {
                        unlink(str_replace(env('APP_URL'), ".", $b->file));
                    }
                }
            }
        }
        unset($data->berkas);
        return response()->json([
            'message' => 'berhasil memperpanjang reklame. harap upload ulang berkas permohonan', 
            'data' => $data
        ], 201);
    }

    public function reklameByNoReg(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'no_reg' => 'required|string'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $data = RegisterReklame::with(['permohonan' => function($q) {
                    $q->with('alur')->with('detail')->with('berkas')->with('kelengkapan')->get();
                }])->where('no_reg', $request->input('no_reg'))->first();

        return response()->json([
            'data' => $data
        ], 200);
    }

    public function validasiSKPD(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'id_reklame' => 'required|integer',
            'statuslunas' => 'required|integer'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $verifikator = DB::table('alur_permohonan')
                            ->where('id_permohonan', $request->input('id_reklame'))
                            ->where('upload', 1)
                            ->first();
        if (auth()->user()->id != $verifikator->id_user) {
            return response()->json(['message'=>'anda tidak memiliki hak akses'], 401);
        }
        ReklamePembayaran::where('id_permohonan', $request->input('id_reklame'))->update(['statuslunas' => $request->input('statuslunas')]);
        $data = ReklamePembayaran::where('id_permohonan', $request->input('id_reklame'))
            ->select('id', 'id_permohonan', 'no_skpd', 'statuslunas', 'fileskpd', 'nominal')
            ->get();
        
        return response()->json([
            'message' => 'berhasil mengubah status lunas SKPD',
            'data' => $data
        ], 200);
    }

    public function uploadRekomendasiPerRegister(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'no_reg' => 'required|string',
            'file' => 'required|mimes:pdf|max:4096'
        ]);
        if($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $reklames = Permohonan::where('no_registrasi', $request->input('no_reg'))->get();
        if (count($reklames) == 0) {
            return response()->json([
                'message' => 'Reklame dengan nomor registrasi yang diinput tidak ada'
            ], 404);
        }
        if ($file = $request->file('file')) {
            foreach ($reklames as $r) {
                if (!is_null($r->surat_rekomendasi)) {
                    if(file_exists(str_replace(env('APP_URL'), ".", $r->surat_rekomendasi))) {
                        unlink(str_replace(env('APP_URL'), ".", $r->surat_rekomendasi));
                    }
                }
            }
            
            $path = $file->store('public/file_suratrekomendasi');
            DB::table('permohonan')
                ->where('no_registrasi', $request->input('no_reg'))
                ->update([
                    'surat_rekomendasi' => env('APP_URL')."/storage/app/public/file_suratrekomendasi/".$file->hashName()
                ]);
            $data = Permohonan::where('no_registrasi', $request->input('no_reg'))->get();

            return response()->json([
                'message'=>'Surat rekomendasi per nomor registrasi berhasil diupload',
                'data' => $data
            ], 201);
        }
    }


    // public function extendReg(Request $request, $id)
    // {
    //     $validator = Validator::make($request->all(),[ 
    //         'expired_date' => 'required|string',
    //     ]);
    //     if($validator->fails()) {          
    //         return response()->json(['error'=>$validator->errors()], 401);
    //     }
    //     $data = RegisterReklame::find($id);
    //     RegisterReklame::where('id', $id)->update([
    //         'expired_date' => $request->input('expired_date'),
    //         'no_reg_lama' => $data->no_reg,
    //         'no_reg' => "BLL/".time()."/9"."/".auth()->user()->id
    //     ]);
        
    //     $newdata = RegisterReklame::find($id);
    //     Permohonan::where('no_registrasi', $data->no_reg)->update([
    //         'no_registrasi' => $newdata->no_reg
    //     ]);
    //     return response()->json([
    //         'message'=>'Perpanjangan masa berlaku register reklame berhasil.',
    //         'data' => $newdata
    //     ], 201);
    // }

    public function updateStatusTidakAktif() {
        $temp = DB::table('permohonan as p')
                ->select('p.id')
                ->leftJoin('detail_permohonan as dp', function($join) {
                    $join->on('dp.id_permohonan', 'p.id')->where('dp.kode_isian', 'TGL_AKHIR');
                })
                ->where('p.id_jenis_izin', 9)
                ->where('p.status', 'Selesai')
                ->whereNull('p.deleted_at')
                ->where('dp.value', '<', DB::raw('DATE(NOW())'))
                ->get();
        $ids = [];
        foreach ($temp as $r) {
            array_push($ids, $r->id);
        }
        DB::table('permohonan')
            ->whereIn('id', $ids)
            ->update(['status' => 'Tidak Aktif']);

        return response()->json([
            'message'=>'update status permohonan reklame ke tidak aktif berhasil',
            'data' => $ids
        ], 201);
    }
}
