<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $temp = DB::table('permohonan as p')
                ->select('p.id')
                ->leftJoin('detail_permohonan as dp', function($join) {
                    $join->on('dp.id_permohonan', 'p.id')->where('dp.kode_isian', 'TGL_AKHIR');
                })
                ->where('p.id_jenis_izin', 9)
                ->where('p.status', 'Selesai')
                ->whereNull('p.deleted_at')
                ->where('dp.value', '<', DB::raw('DATE(NOW())'))
                ->get()->toArray();
            $ids = [];
            foreach ($temp as $r) {
                array_push($ids, $r->id);
            }
            DB::table('permohonan')
                ->whereIn('id', $ids)
                ->update(['status' => 'Tidak Aktif']);
        })->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
