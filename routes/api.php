<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JWTController;
use App\Http\Controllers\JenisIzinController;
use App\Http\Controllers\DetailFormController;
use App\Http\Controllers\PermohonanController;
use App\Http\Controllers\AlurPermohonanController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\KuisionerController;
use App\Http\Controllers\ReklameController;

Route::get('unauthenthicated', function() {
    return response()->json(["message" => "unauthenthicated."], 200);
});
Route::group(['middleware' => 'api'], function($router) {
    Route::post('/register', [JWTController::class, 'register']);
    Route::post('/login', [JWTController::class, 'login']);
    Route::post('/logout', [JWTController::class, 'logout']);
    Route::post('/refresh', [JWTController::class, 'refresh']);
    Route::get('/profile', [JWTController::class, 'profile']);
    Route::get('/users', [JWTController::class, 'getUsers']);
    Route::post('/users/send-email-verification', [JWTController::class, 'verifyEmail']);
    Route::post('/verify-email-user/{id}', [JWTController::class, 'verifyUser']);
    Route::post('/users/send-email-reset-password', [JWTController::class, 'resetPasswordMail']);
    Route::post('/users/reset-password', [JWTController::class, 'resetPassword']);
});

Route::group(['prefix' => 'jenis-izin','middleware' => 'api'], function($router) {
    Route::get('', [JenisIzinController::class, 'index']);
    Route::get('view/{id}', [JenisIzinController::class, 'view']);
    Route::post('', [JenisIzinController::class, 'store']);
    Route::post('update', [JenisIzinController::class, 'update']);
    Route::post('delete', [JenisIzinController::class, 'delete']);
    Route::post('view/{id}/upload-template', [JenisIzinController::class, 'uploadTemplate']);
});

Route::group(['prefix' => 'permohonan','middleware' => 'api'], function($router) {
    Route::get('', [PermohonanController::class, 'index']);
    Route::get('view/{id}', [PermohonanController::class, 'view']);
    Route::get('view-by-qr/{kode}', [PermohonanController::class, 'viewByQR']);
    Route::post('', [PermohonanController::class, 'store']);
    Route::put('update', [PermohonanController::class, 'update']);
    Route::delete('delete/{id}', [PermohonanController::class, 'delete']);
    Route::post('upload-suratkuasa', [PermohonanController::class, 'uploadSuratKuasa']);
    Route::post('upload-berkas', [PermohonanController::class, 'uploadBerkas']);
    Route::post('update-status', [PermohonanController::class, 'setStatus']);
    Route::post('test-upload', [PermohonanController::class, 'testUpload']);
    Route::post('delete-file', [PermohonanController::class, 'deleteFile']);
    Route::post('view/{id}/upload-pdf', [PermohonanController::class, 'uploadPDFTemplate']);
    Route::get('download-template', [PermohonanController::class, 'downloadTemplateWord']);
    Route::post('upload-extraimg', [PermohonanController::class, 'uploadExtraImage']);
    // Route::get('send-email', [PermohonanController::class, 'sendEmail']);
    Route::post('send-email-izindiajukan', [PermohonanController::class, 'emailIzinDiajukan']);
    Route::delete('berkas/{id}', [PermohonanController::class, 'deleteBerkas']);
    Route::put('alur/revisi', [AlurPermohonanController::class, 'revisi']);
    Route::put('alur/revisi-batal', [AlurPermohonanController::class, 'pembatalanRevisi']);
    Route::post('alur/set-status', [AlurPermohonanController::class, 'setStatus']);
    Route::put('alur/validasi-berkas', [AlurPermohonanController::class, 'validasiBerkas']);
    Route::put('alur/isi-kelengkapan', [AlurPermohonanController::class, 'isiKelengkapan']);
    Route::post('alur/upload-rekomendasi', [AlurPermohonanController::class, 'uploadRekomendasi']);
    Route::post('alur/sign-dokumen', [AlurPermohonanController::class, 'signDokumen']);
    Route::get('get-by-id-jenis', [PermohonanController::class, 'getByJenisIzin']);
    Route::get('laporan-bulanan', [PermohonanController::class, 'laporanBulanan']);
});

Route::group(['prefix' => 'administrator','middleware' => 'api'], function($router) {
    Route::put('rollback-alur', [AdminController::class, 'rollbackAlurPermohonan']);
    Route::put('reset-permohonan', [AdminController::class, 'resetPermohonan']);
});

Route::group(['prefix' => 'master-user','middleware' => 'api'], function($router) {
    Route::get('', [UserController::class, 'index']);
    Route::put('/update', [UserController::class, 'update']);
    Route::delete('/delete/{id}', [UserController::class, 'delete']);
    Route::put('/change-password', [UserController::class, 'updatePassword']);
});

Route::group(['prefix' => 'kuisioner','middleware' => 'api'], function($router) {
    Route::get('', [KuisionerController::class, 'index']);
    Route::post('', [KuisionerController::class, 'save']);
    Route::post('skm', [KuisionerController::class, 'saveSKM']);
    Route::get('laporan', [KuisionerController::class, 'laporanSurvey']);
});

Route::group(['prefix' => 'reklame', 'middleware' => 'api'], function($router) {
    Route::get('', [ReklameController::class, 'index']);
    Route::get('view/{id}', [ReklameController::class, 'view']);
    Route::post('', [ReklameController::class, 'save']);
    Route::put('update-registrasi/{id}', [ReklameController::class, 'update']);
    Route::delete('delete-registrasi/{id}', [ReklameController::class, 'delete']);
    Route::post('add-reklame', [ReklameController::class, 'saveReklame']);
    Route::put('update-reklame/{id}', [ReklameController::class, 'updateReklame']);
    Route::delete('delete-reklame/{id}', [ReklameController::class, 'deleteReklame']);
    Route::post('add-reklame', [ReklameController::class, 'saveReklame']);
    Route::put('kelengkapan', [ReklameController::class, 'isiKelengkapanPerRegistrasi']);
    Route::put('kelengkapan-reklame', [ReklameController::class, 'isiKelengkapanPerReklame']);
    Route::post('upload-berkas', [ReklameController::class, 'uploadBerkasPerRegistrasi']);
    Route::post('upload-skpd', [ReklameController::class, 'uploadSKPD']);
    Route::get('jumlah-per-status', [ReklameController::class, 'countByStatus']);
    Route::get('list-reklame', [ReklameController::class, 'listReklame']);
    Route::get('list-reklame-expired', [ReklameController::class, 'listReklameExpired']);
    Route::get('posisi-reklame', [ReklameController::class, 'listReklamePosition']);
    Route::post('claim-register', [ReklameController::class, 'claimRegister']);
    Route::post('perpanjang-reklame', [ReklameController::class, 'perpanjangReklame']);
    Route::get('list-reklame-by-noreg', [ReklameController::class, 'reklameByNoReg']);
    Route::post('validasi-skpd', [ReklameController::class, 'validasiSKPD']);
    Route::post('upload-surat-rekomendasi-register', [ReklameController::class, 'uploadRekomendasiPerRegister']);
    // Route::post('update-status-tidak-aktif-test', [ReklameController::class, 'updateStatusTidakAktif']);

    // Route::post('extend-reg/{id}', [ReklameController::class, 'extendReg']);
});